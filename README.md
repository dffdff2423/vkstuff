# Misc vulkan stuff

Some projects I am working on to learn graphics programming with vulkan.

# Copyright

Most files are under the folowing copyright unless otherwise noted:
    SPDX-License-Identifier: GPL-3.0-only

    Copyright (C) 2022 dffdff2423

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

Files that are not covered under the previous notice will have a copyright
notice at the top of the file, or if this is not possible the copyright will be
listed inside a file called CREDITS.txt at the root of the module the file is in.
