//! Vulkan synchronization primitives

use std::sync::Arc;

use ash::prelude::VkResult;
use ash::vk;

use smallvec::SmallVec;

use crate::{
    impl_nameable_for_type,
    Device,
};

/// GPU to GPU synchronization primitive, wraps [`vk::Semaphore`]
pub struct Semaphore {
    pub(crate) handle: vk::Semaphore,
    dev: Arc<Device>,
}

impl Semaphore {
    pub fn new(dev: Arc<crate::Device>) -> VkResult<Semaphore> {
        let info = vk::SemaphoreCreateInfo {
            ..Default::default()
        };
        let sem = unsafe { dev.handle.create_semaphore(&info, None)? };
        Ok(Semaphore { handle: sem, dev })
    }

    pub fn handle(&self) -> vk::Semaphore {
        self.handle
    }
}

impl Drop for Semaphore {
    fn drop(&mut self) {
        unsafe {
            self.dev.handle().destroy_semaphore(self.handle, None);
        }
    }
}

/// GPU to CPU synchronization primitive, wraps [`vk::Fence`]
pub struct Fence {
    pub(crate) handle: vk::Fence,
    dev: Arc<Device>,
}

impl Fence {
    pub fn new(dev: Arc<crate::Device>, flags: vk::FenceCreateFlags) -> VkResult<Fence> {
        let info = vk::FenceCreateInfo {
            flags,
            ..Default::default()
        };
        let fence = unsafe { dev.handle.create_fence(&info, None)? };
        Ok(Fence { handle: fence, dev })
    }

    /// wait for just this fence
    pub fn wait_for(&self, timeout: u64) -> VkResult<()> {
        unsafe { self.dev.handle.wait_for_fences(&[self.handle], true, timeout) }
    }

    pub fn reset(&self) -> VkResult<()> {
        unsafe { self.dev.handle.reset_fences(&[self.handle]) }
    }

    pub fn handle(&self) -> vk::Fence {
        self.handle
    }
}

impl Drop for Fence {
    fn drop(&mut self) {
        unsafe {
            self.dev.handle.destroy_fence(self.handle, None);
        }
    }
}

/// Allows waiting on and reseting multiple fences at the same time
pub trait WaitForFences {
    /// Waits for fences, all fences must have been created from the same Device
    /// and in a state where they can be waited on for it to be safe.
    ///
    /// # Safety
    /// It all of the fences must be from the same [`Device`]
    unsafe fn wait_for_fences(&self, wait_all: bool, timeout: u64) -> VkResult<()>;

    /// Resets all fences, all fences must have been created from the same Device
    /// and resetable for it to be safe.
    ///
    /// # Safety
    /// It all of the fences must be from the same [`Device`]
    unsafe fn reset_fences(&self) -> VkResult<()>;
}

impl WaitForFences for [Fence] {
    unsafe fn wait_for_fences(&self, wait_all: bool, timeout: u64) -> VkResult<()> {
        if self.is_empty() {
            return Ok(());
        }

        let handles = self.iter().map(|f| f.handle).collect::<SmallVec<[vk::Fence; 4]>>();

        let dev = &self[0].dev;
        dev.handle.wait_for_fences(&handles, wait_all, timeout)
    }

    unsafe fn reset_fences(&self) -> VkResult<()> {
        if self.is_empty() {
            return Ok(());
        }

        let handles = self.iter().map(|f| f.handle).collect::<SmallVec<[vk::Fence; 4]>>();

        let dev = &self[0].dev;
        dev.handle.reset_fences(&handles)
    }

}

impl WaitForFences for Fence {
    unsafe fn wait_for_fences(&self, _wait_all: bool, timeout: u64) -> VkResult<()> {
        self.wait_for(timeout)
    }
    unsafe fn reset_fences(&self) -> VkResult<()> {
        self.reset()
    }

}

impl_nameable_for_type!(Semaphore, vk::ObjectType::SEMAPHORE);
impl_nameable_for_type!(Fence, vk::ObjectType::FENCE);
