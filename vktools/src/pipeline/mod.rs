//! Everything related to vulkan piplines

pub mod uniform;

use std::{
    collections::{BTreeMap, HashMap},
    error::Error,
    fmt,
    io::{self, prelude::*},
    mem::size_of,
    ptr::null,
    sync::Arc,
};

use ash::prelude::*;
use ash::vk;

use smallvec::SmallVec;

use crate::{
    cstr, to_void,
    CommandBuffer, Device,
    impl_nameable_for_type,
    util::to_byte_slice,
};

#[derive(Debug)]
pub enum ShaderModuleCreationError {
    VkError(vk::Result),
    IoError(io::Error),
}

impl fmt::Display for ShaderModuleCreationError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::VkError(e) => e.fmt(f),
            Self::IoError(e) => e.fmt(f),
        }
    }
}

impl Error for ShaderModuleCreationError {}

impl From<vk::Result> for ShaderModuleCreationError {
    fn from(r: vk::Result) -> Self {
        Self::VkError(r)
    }
}

impl From<io::Error> for ShaderModuleCreationError {
    fn from(e: io::Error) -> Self {
        Self::IoError(e)
    }
}

/// Repersents a SPIRV shader module
pub struct ShaderModule {
    handle: vk::ShaderModule,
    dev: Arc<Device>,
    stage: vk::ShaderStageFlags,
    spv: Vec<u32>,
}

impl ShaderModule {
    pub fn from_reader<T: Read + Seek>(
        dev: Arc<Device>,
        stage: vk::ShaderStageFlags,
        input: &mut T,
    ) -> Result<Self, ShaderModuleCreationError> {
        let spv = ash::util::read_spv(input)?;
        let info = vk::ShaderModuleCreateInfo {
            code_size: size_of::<u32>() * spv.len(),
            p_code: spv.as_ptr(),
            ..Default::default()
        };

        let module = unsafe { dev.handle.create_shader_module(&info, None)? };

        Ok(Self {
            handle: module,
            dev,
            stage,
            spv,
        })
    }
}

impl Drop for ShaderModule {
    fn drop(&mut self) {
        unsafe {
            self.dev.handle.destroy_shader_module(self.handle, None);
        }
    }
}

type ReflectResult<T> = Result<T, rspirv_reflect::ReflectError>;

#[derive(PartialEq, Eq, Clone, Copy, Hash)]
pub struct PushConstentInfo {
    pub offset: u32,
    pub size: u32,
    pub stage: vk::ShaderStageFlags,
}

pub fn cauclulate_shader_information(shaders: &[ShaderModule])
    -> ReflectResult<(Option<PushConstentInfo>, SmallVec<[DescriptorSetLayoutInfo; 4]>)>
{
    let mut all_push_const = None;
    let mut sets: BTreeMap<u32, (vk::ShaderStageFlags, BTreeMap<u32, rspirv_reflect::DescriptorInfo>)> = BTreeMap::new();
    for shader in shaders {
        let reflect = rspirv_reflect::Reflection::new_from_spirv(to_byte_slice(&shader.spv))?;
        let desc_sets = reflect.get_descriptor_sets()?;

        for (k, v) in desc_sets {
            if let Some(set) = sets.get_mut(&k) {
                if v != set.1 {
                    panic!("Shaders have differing descriptor sets");
                }
                set.0 |= shader.stage;
            } else {
                sets.insert(k, (shader.stage, v));
            }
        }

        let push_const = reflect.get_push_constant_range()?;

        if all_push_const.is_none() {
            all_push_const = push_const.map(|inner| PushConstentInfo { offset: inner.offset, size: inner.size, stage: shader.stage });
        } else {
            let old_push = all_push_const.as_mut().unwrap();
            if let Some(new_push) = push_const {
                if old_push.size != new_push.size || old_push.offset != new_push.offset {
                    panic!("Shader stages have differing push constants");
                }
                old_push.stage |= shader.stage;
            }
        }
    }

    if sets.len() > 4 {
        panic!("Does not support more than 4 descriptor sets");
    }

    let sets: SmallVec<[(vk::ShaderStageFlags, BTreeMap<u32, rspirv_reflect::DescriptorInfo>); 4]>
        = sets.into_values().collect();
    let sets: SmallVec<[DescriptorSetLayoutInfo; 4]> = sets.into_iter().map(|(shader, set)| {
        let bindings = set.into_values().enumerate().map(|(i, binding)| {
            use rspirv_reflect::BindingCount;
            vk::DescriptorSetLayoutBinding {
                binding: i as u32,
                descriptor_type: match vk::DescriptorType::from_raw(binding.ty.0 as i32) {
                    vk::DescriptorType::UNIFORM_BUFFER => vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
                    t => t,
                },
                descriptor_count: match binding.binding_count {
                    BindingCount::One => 1,
                    BindingCount::StaticSized(s) => s,
                    BindingCount::Unbounded => todo!("Unbounded descriptor not supported yet"),
                } as u32,
                stage_flags: shader,
                p_immutable_samplers: null(),
            }
        }).collect();
        DescriptorSetLayoutInfo {
            bindings,
        }
    }).collect();

    Ok((all_push_const, sets))
}

/// Wraps [`vk::Pipeline`]
pub struct PipelineLayout {
    pub(crate) handle: vk::PipelineLayout,
    dev: Arc<Device>,
}

impl PipelineLayout {
    pub fn new(
        dev: Arc<Device>,
        descriptor_layouts: &[vk::DescriptorSetLayout],
        push_const: Option<PushConstentInfo>,
    ) -> VkResult<PipelineLayout> {
        let push_const = push_const.map(|inner| vk::PushConstantRange { size: inner.size, offset: inner.offset, stage_flags: inner.stage });
        let mut info = vk::PipelineLayoutCreateInfo {
            set_layout_count: descriptor_layouts.len() as u32,
            p_set_layouts: descriptor_layouts.as_ptr(),
            ..Default::default()
        };
        if let Some(push) = &push_const {
            info.push_constant_range_count = 1;
            info.p_push_constant_ranges = push
        }

        let handle = unsafe { dev.handle.create_pipeline_layout(&info, None)? };
        Ok(PipelineLayout {
            handle, dev
        })
    }

    #[inline]
    pub fn handle(&self) -> vk::PipelineLayout {
        self.handle
    }
}

impl Drop for PipelineLayout {
    fn drop(&mut self) {
        unsafe {
            self.dev.handle.destroy_pipeline_layout(self.handle, None);
        }
    }
}

/// Describes vertex bindings
pub struct VertexInputDescription {
    pub bindings: Vec<vk::VertexInputBindingDescription>,
    pub attr: Vec<vk::VertexInputAttributeDescription>,
    pub flags: vk::PipelineVertexInputStateCreateFlags,
}

/// Builder for a vulkan graphics pipeline
///
/// Always uses dynamic state for the viewport and scissor
pub struct RasterizationPipelineBuilder<'a> {
    shader_stages: Vec<&'a ShaderModule>,
    input_assembly: Option<vk::PipelineInputAssemblyStateCreateInfo>,
    rasterization_state: Option<vk::PipelineRasterizationStateCreateInfo>,
    multisample_state: Option<vk::PipelineMultisampleStateCreateInfo>,
    color_blend_attachment: Option<vk::PipelineColorBlendAttachmentState>,
    layout: Option<&'a PipelineLayout>,
    vertex_input: Option<VertexInputDescription>,
}

impl<'a> RasterizationPipelineBuilder<'a> {
    pub fn add_shader_stage(&mut self, shader: &'a ShaderModule) -> &mut Self {
        self.shader_stages.push(shader);
        self
    }

    pub fn clear_shader_stages(&mut self) -> &mut Self {
        self.shader_stages.clear();
        self
    }

    pub fn set_shader_stages(&mut self, stages: Vec<&'a ShaderModule>) -> &mut Self {
        self.shader_stages = stages;
        self
    }

    pub fn with_input_assembly(&mut self, topology: vk::PrimitiveTopology) -> &mut Self {
        let info = vk::PipelineInputAssemblyStateCreateInfo {
            topology,
            primitive_restart_enable: vk::FALSE,
            ..Default::default()
        };

        self.input_assembly = Some(info);
        self
    }

    pub fn with_rasterization_state(
        &mut self,
        polygon_mode: vk::PolygonMode,
        cull_mode: vk::CullModeFlags,
    ) -> &mut Self {
        let info = vk::PipelineRasterizationStateCreateInfo {
            rasterizer_discard_enable: vk::FALSE,
            depth_clamp_enable: vk::FALSE,
            polygon_mode,
            line_width: 1.0f32, // vulkan only supports a line with of 1.0 unless the wide lines feature is enabled
            cull_mode,
            front_face: vk::FrontFace::CLOCKWISE,

            // diable this for now
            depth_bias_enable: vk::FALSE,
            depth_bias_clamp: 0.0,
            depth_bias_slope_factor: 0.0,
            depth_bias_constant_factor: 0.0,

            flags: vk::PipelineRasterizationStateCreateFlags::empty(), // there are no flags
            ..Default::default()
        };

        self.rasterization_state = Some(info);
        self
    }

    /// Curretnly does not support multisampling
    pub fn with_multisample_state(&mut self) -> &mut Self {
        let info = vk::PipelineMultisampleStateCreateInfo {
            sample_shading_enable: vk::FALSE,
            rasterization_samples: vk::SampleCountFlags::TYPE_1,
            min_sample_shading: 1.0,
            p_sample_mask: null(),
            alpha_to_coverage_enable: vk::FALSE,
            alpha_to_one_enable: vk::FALSE,

            flags: vk::PipelineMultisampleStateCreateFlags::empty(), // there are no flags
            ..Default::default()
        };

        self.multisample_state = Some(info);
        self
    }

    // this should be more customizable
    pub fn with_color_blend_attachment(&mut self, permit_opacity: bool, /* FIXME */ particle_func: bool) -> &mut Self {
        let mut attachment_state = vk::PipelineColorBlendAttachmentState {
            color_write_mask: vk::ColorComponentFlags::RGBA,
            blend_enable: vk::FALSE,
            ..Default::default()
        };
        if permit_opacity {
            attachment_state.blend_enable = vk::TRUE;
            attachment_state.src_color_blend_factor = vk::BlendFactor::SRC_ALPHA;
            attachment_state.dst_color_blend_factor = if !particle_func { vk::BlendFactor::ONE_MINUS_SRC_ALPHA}
                                                          else { vk::BlendFactor::ONE };
            attachment_state.color_blend_op = vk::BlendOp::ADD;
            attachment_state.src_alpha_blend_factor = vk::BlendFactor::ZERO;
            attachment_state.dst_alpha_blend_factor = vk::BlendFactor::ONE;
            attachment_state.alpha_blend_op = vk::BlendOp::ADD;
        }
        self.color_blend_attachment = Some(attachment_state);
        self
    }

    pub fn with_layout(&mut self, layout: &'a PipelineLayout) -> &mut Self {
        self.layout = Some(layout);
        self
    }

    pub fn with_vertex_input(&mut self, input: VertexInputDescription) -> &mut Self {
        self.vertex_input = Some(input);
        self
    }

    /// Build the pipeline
    ///
    /// Will panic if the following methods where not called:
    /// [`Self::with_input_assembly`]
    /// [`Self::with_rasterization_state`]
    /// [`Self::with_multisample_state`]
    /// [`Self::with_color_blend_attachment`]
    /// [`Self::with_layout`]
    pub fn build(
        &mut self,
        dev: Arc<Device>,
        color_attachment_format: vk::Format,
    ) -> VkResult<RasterizationPipeline> {
        assert!(self.input_assembly.is_some(), "you must call RasterizationPipelineBuilder::with_input_assembly");
        assert!(self.rasterization_state.is_some(), "you must call RasterizationPipelineBuilder::with_rasterization_state");
        assert!(self.multisample_state.is_some(), "you must call RasterizationPipelineBuilder::with_multisample_state");
        assert!(self.color_blend_attachment.is_some(), "you must call RasterizationPipelineBuilder::with_color_blend_attachment");
        assert!(self.layout.is_some(), "you must call RasterizationPipelineBuilder::with_layout");

        let input_assembly         = self.input_assembly.unwrap();
        let rasterization_state    = self.rasterization_state.unwrap();
        let multisample_state      = self.multisample_state.unwrap();
        let color_blend_attachment = self.color_blend_attachment.unwrap();
        let layout                 = self.layout.as_ref().unwrap();

        let shader_stages = self.shader_stages.iter().map(|s| {
            vk::PipelineShaderStageCreateInfo {
                stage: s.stage,
                module: s.handle,
                p_name: cstr!("main"),
                p_specialization_info: null(),

                ..Default::default() // flags are set to empty
            }
        }).collect::<Vec<_>>();

        let vertex_info = match &self.vertex_input {
            Some(i) => vk::PipelineVertexInputStateCreateInfo {
                        vertex_binding_description_count: i.bindings.len() as u32,
                        p_vertex_binding_descriptions: i.bindings.as_ptr(),

                        vertex_attribute_description_count: i.attr.len() as u32,
                        p_vertex_attribute_descriptions: i.attr.as_ptr(),

                        flags: i.flags,

                        ..Default::default()
            },
            None => vk::PipelineVertexInputStateCreateInfo::default(),
        };


        // set dynamicly so we just need to set the count
        let viewport_state = vk::PipelineViewportStateCreateInfo {
            viewport_count: 1,
            scissor_count: 1,

            ..Default::default()
        };

        let color_blending = vk::PipelineColorBlendStateCreateInfo {
            logic_op_enable: vk::FALSE,
            logic_op: vk::LogicOp::COPY,

            attachment_count: 1,
            p_attachments: &color_blend_attachment,

            ..Default::default()
        };

        const DYNAMIC_STATES: &[vk::DynamicState] = &[
            vk::DynamicState::VIEWPORT,
            vk::DynamicState::SCISSOR,
        ];

        let dynamic_state_info = vk::PipelineDynamicStateCreateInfo {
            dynamic_state_count: DYNAMIC_STATES.len() as u32,
            p_dynamic_states: DYNAMIC_STATES.as_ptr(),
            ..Default::default()
        };

        let dynamic_rendering_info = vk::PipelineRenderingCreateInfoKHR {
            color_attachment_count: 1,
            p_color_attachment_formats: &color_attachment_format,
            ..Default::default()
        };

        let info = vk::GraphicsPipelineCreateInfo {
            p_next: to_void(&dynamic_rendering_info),

            stage_count: shader_stages.len() as u32,
            p_stages: shader_stages.as_ptr(),

            p_vertex_input_state: &vertex_info,
            p_input_assembly_state: &input_assembly,

            p_viewport_state: &viewport_state,
            p_rasterization_state: &rasterization_state,
            p_multisample_state: &multisample_state,
            p_color_blend_state: &color_blending,
            p_dynamic_state: &dynamic_state_info,
            layout: layout.handle,
            render_pass: vk::RenderPass::null(),
            subpass: 0,
            base_pipeline_handle: vk::Pipeline::null(),

            ..Default::default()
        };

        // TODO: make multiple pipelines at the same time
        // https://www.khronos.org/registry/vulkan/specs/1.3/html/chap10.html#pipelines-multiple

        let pipeline = unsafe { dev.handle.create_graphics_pipelines(
                vk::PipelineCache::null(), &[info], None) };
        let pipeline = match pipeline {
            Ok(val) => val[0],
            Err(e) => return Err(e.1),
        };

        Ok(RasterizationPipeline {
            handle: pipeline,
            dev,
        })
    }
}

/// Wrapper for [`vk::Pipeline`]
pub struct RasterizationPipeline {
    handle: vk::Pipeline,
    dev: Arc<Device>,
}

impl RasterizationPipeline {
    pub fn builder() -> RasterizationPipelineBuilder<'static> {
        RasterizationPipelineBuilder {
            shader_stages: Vec::new(),
            vertex_input: None,
            input_assembly: None,
            rasterization_state: None,
            multisample_state: None,
            color_blend_attachment: None,
            layout: None,
        }
    }

    /// Add the bind pipeline command to the command buffer with this pipline as the value
    ///
    /// # Safety
    /// * The [`CommandBuffer`] must be in recording mode
    /// * The dynamic state must have been set before this is called
    pub unsafe fn cmd_bind(&self, buffer: &CommandBuffer) {
        self.dev.handle.cmd_bind_pipeline(buffer.handle, vk::PipelineBindPoint::GRAPHICS, self.handle);
    }
}

impl Drop for RasterizationPipeline {
    fn drop(&mut self) {
        unsafe {
            self.dev.handle.destroy_pipeline(self.handle, None);
        }
    }
}

const DESCRIPTOR_SIZES: &[(vk::DescriptorType, f64)] = &[
    (vk::DescriptorType::SAMPLER, 0.5),
    (vk::DescriptorType::COMBINED_IMAGE_SAMPLER, 4.0),
    (vk::DescriptorType::SAMPLED_IMAGE, 4.0),
    (vk::DescriptorType::STORAGE_IMAGE, 1.0),
    (vk::DescriptorType::UNIFORM_TEXEL_BUFFER, 1.0),
    (vk::DescriptorType::STORAGE_TEXEL_BUFFER, 1.0),
    (vk::DescriptorType::UNIFORM_BUFFER, 2.0),
    (vk::DescriptorType::STORAGE_BUFFER, 2.0),
    (vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC, 1.0),
    (vk::DescriptorType::STORAGE_BUFFER_DYNAMIC, 1.0),
    (vk::DescriptorType::INPUT_ATTACHMENT, 0.5)
];

pub struct DescriptorAllocator {
    dev: Arc<Device>,
    current_pool: Option<vk::DescriptorPool>,
    used_pools: Vec<vk::DescriptorPool>,
    free_pools: Vec<vk::DescriptorPool>,
}

impl DescriptorAllocator {
    pub fn new(dev: Arc<Device>) -> Self {
        Self {
            dev,
            current_pool: None,
            used_pools: Vec::new(),
            free_pools: Vec::new(),
        }
    }

    fn get_pool(&mut self) -> VkResult<vk::DescriptorPool> {
        if self.free_pools.len() > 0 {
            return Ok(self.free_pools.pop().unwrap());
        } else {
            return create_descriptor_pool(&self.dev, 1000, vk::DescriptorPoolCreateFlags::empty());
        }
    }

    pub unsafe fn reset_pool(&mut self) -> VkResult<()> {
        for pool in self.used_pools.iter() {
            self.dev.handle.reset_descriptor_pool(*pool, vk::DescriptorPoolResetFlags::empty())?;
            self.free_pools.push(*pool);
        }

        self.used_pools.clear();
        self.current_pool = None;

        Ok(())
    }

    pub unsafe fn allocate(&mut self, layouts: &[vk::DescriptorSetLayout]) -> VkResult<Vec<vk::DescriptorSet>> {
        if self.current_pool == None {
            self.current_pool = Some(self.get_pool()?);
            self.used_pools.push(self.current_pool.unwrap());
        }

        let alloc_info = vk::DescriptorSetAllocateInfo {
            p_set_layouts: layouts.as_ptr(),
            descriptor_pool: self.current_pool.unwrap(),
            descriptor_set_count: layouts.len() as u32,
            ..Default::default()
        };

        let alloc_res = self.dev.handle.allocate_descriptor_sets(&alloc_info);
        let realloc_needed;

        match alloc_res {
            //Ok(d) => return Ok(d.into_iter().enumerate().map(|(i, s)| DescriptorSet { dev: Arc::clone(&self.dev), inner: s}).collect()),
            Ok(d) => return Ok(d),
            Err(e) => match e {
                vk::Result::ERROR_FRAGMENTED_POOL
                | vk::Result::ERROR_OUT_OF_POOL_MEMORY => {
                    realloc_needed = true;
                }
                e =>  return Err(e),
            }
        }

        if realloc_needed {
            self.current_pool = Some(self.get_pool()?);
            self.used_pools.push(self.current_pool.unwrap());
            let res = self.dev.handle.allocate_descriptor_sets(&alloc_info); //?.into_iter().map(|s| {
                //DescriptorSet { dev: Arc::clone(&self.dev), inner: s }
            //}).collect();
            return res;
        }

        unreachable!();
    }
}

fn create_descriptor_pool(dev: &Device, count: u32, flags: vk::DescriptorPoolCreateFlags) -> VkResult<vk::DescriptorPool> {
    let sizes = DESCRIPTOR_SIZES.iter().map(|s| {
        vk::DescriptorPoolSize {
            ty: s.0,
            descriptor_count: (s.1 * count as f64) as u32,
        }
    }).collect::<Vec<_>>();

    let pool_info = vk::DescriptorPoolCreateInfo {
        flags,
        pool_size_count: sizes.len() as u32,
        p_pool_sizes: sizes.as_ptr(),
        max_sets: count,
        ..Default::default()
    };

    unsafe { dev.handle.create_descriptor_pool(&pool_info, None) }
}

impl Drop for DescriptorAllocator {
    fn drop(&mut self) {
        unsafe {
            for pool in self.free_pools.iter() {
                self.dev.handle.destroy_descriptor_pool(*pool, None);
            }
            for pool in self.used_pools.iter() {
                self.dev.handle.destroy_descriptor_pool(*pool, None);
            }
        }
    }
}

pub struct DescriptorSetLayoutInfo {
    pub bindings: SmallVec<[vk::DescriptorSetLayoutBinding; 4]>,
}

impl std::hash::Hash for DescriptorSetLayoutInfo {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        for b in self.bindings.iter() {
            b.binding.hash(state);
            b.stage_flags.hash(state);
            b.descriptor_type.hash(state);
            b.descriptor_count.hash(state);
            b.p_immutable_samplers.hash(state);
        }
    }
}

impl std::cmp::PartialEq for DescriptorSetLayoutInfo {
    fn eq(&self, other: &Self) -> bool {
        for (i, b) in self.bindings.iter().enumerate() {
            if b.binding != other.bindings[i].binding {
                return false;
            }
            if b.stage_flags != other.bindings[i].stage_flags {
                return false;
            }
            if b.descriptor_type != other.bindings[i].descriptor_type {
                return false;
            }
            if b.descriptor_count != other.bindings[i].descriptor_count {
                return false;
            }
            if b.p_immutable_samplers != other.bindings[i].p_immutable_samplers {
                return false;
            }
        }

        return true;
    }
}

impl std::cmp::Eq for DescriptorSetLayoutInfo {}

pub struct DescriptorLayoutCache {
    dev: Arc<Device>,
    layouts: HashMap<DescriptorSetLayoutInfo, vk::DescriptorSetLayout>
}

impl DescriptorLayoutCache {
    pub fn new(dev: Arc<Device>) -> Self {
        Self {
            dev,
            layouts: HashMap::new(),
        }
    }

    pub unsafe fn create_layout(&mut self, mut cache_info: DescriptorSetLayoutInfo) -> VkResult<vk::DescriptorSetLayout> {

        let mut is_sorted = true;
        let mut last_binding: i64 = -1;
        for binding in cache_info.bindings.iter() {
            let binding: &vk::DescriptorSetLayoutBinding = binding;
            if binding.binding as i64 > last_binding {
                last_binding = binding.binding.into();
            } else {
                is_sorted = false;
                break;
            }
        }

        if !is_sorted {
            cache_info.bindings.sort_by_key(|b: &vk::DescriptorSetLayoutBinding| b.binding);
        }

        if let Some(l) = self.layouts.get(&cache_info) {
            return Ok(*l);
        } else {
            let info = vk::DescriptorSetLayoutCreateInfo {
                binding_count: cache_info.bindings.len() as u32,
                p_bindings: cache_info.bindings.as_ptr(),
                ..Default::default()
            };
            let handle = self.dev.handle.create_descriptor_set_layout(&info, None)?;
            self.layouts.insert(cache_info, handle);
            return Ok(handle);
        }
    }
}

impl Drop for DescriptorLayoutCache {
    fn drop(&mut self) {
        unsafe {
            for (_, layout) in self.layouts.iter() {
                self.dev.handle.destroy_descriptor_set_layout(*layout, None);
            }
        }
    }
}

impl_nameable_for_type!(ShaderModule, vk::ObjectType::SHADER_MODULE);
impl_nameable_for_type!(RasterizationPipeline, vk::ObjectType::PIPELINE);

pub trait Vertex {
    fn get_input_description() -> VertexInputDescription;
}
