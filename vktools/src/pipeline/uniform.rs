//! Uniform Buffer utilities.

use std::{
    sync::Arc,
    mem::size_of,
    marker::PhantomData
};

use ash::{vk, prelude::*};

use crate::{
    mem::{Buffer, Allocator, MemoryUsage},
    util::{pad_uniform_buffer_size, to_byte_slice},
};

/// Uniform Buffer contents
///
/// # Safety
/// Structs that implment this must be `#[reper(C)]` and align with the corresponding shader
pub unsafe trait UniformContents {}

unsafe impl UniformContents for () { }

/// `UNIFORM_BUFFER_DYNAMIC` with contents `T`.
pub struct UniformBufferDynamic<T: UniformContents> {
    buffer: Buffer,
    min_allign: vk::DeviceSize,

    _marker: std::marker::PhantomData<T>,
}

impl<T: UniformContents> UniformBufferDynamic<T> {
    pub fn new(alloc: Arc<Allocator>, frames: u64) -> VkResult<Self> {
        assert!(size_of::<T>() <= 16384
                , "uniform should be less than 16384 bytes because acording to vulkan.gpuinfo.org 99.99% of gpus support uniforms greater than this size");
        let min_allign = alloc.dev().properties().limits.min_uniform_buffer_offset_alignment;
        let mut buffer = unsafe { Buffer::new(alloc,
                                 MemoryUsage::CpuToGpu,
                                 vk::BufferUsageFlags::UNIFORM_BUFFER,
                                 frames * pad_uniform_buffer_size(min_allign,
                                                    size_of::<T>() as u64)) }?;
        buffer.map()?;
        Ok(Self { buffer, min_allign, _marker: PhantomData })
    }

    pub unsafe fn write_contents(
        &mut self,
        contents: T,
        frame: usize
    ) {
        self.buffer.copy_to_with_offset(
                    frame * pad_uniform_buffer_size(self.min_allign, size_of::<T>() as u64) as usize,
                    to_byte_slice(&[contents]),
        );
    }

    #[inline]
    pub fn buffer(&self) -> &Buffer {
        &self.buffer
    }

    #[inline]
    pub fn range(&self) -> vk::DeviceSize {
        size_of::<T>() as vk::DeviceSize
    }
}
