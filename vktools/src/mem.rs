//! Vulkan memory constructs including allocators, buffers and images

use std::{
    sync::Arc,
    ptr::{self, null_mut},
};

use ash::prelude::*;
use ash::vk;

use vma::Allocation;
use vma::Alloc;
/// Re-export of [`vk_mem::MemoryUsage`].
pub use vma::MemoryUsage;
/// Re-export of [`vk_mem::AllocationInfo`].
pub use vma::AllocationInfo;

use crate::{
    CommandBuffer,
    Device,
    impl_nameable_for_type,
};

/// A VMA allocator, wraps [`vk_mem::Allocator`]
pub struct Allocator {
    inner: vma::Allocator,
    dev: Arc<Device>
}

impl Allocator {
    pub fn new(dev: Arc<Device>) -> VkResult<Self> {
        let info = vma::AllocatorCreateInfo::new(&dev.instance.instance, &dev.handle, dev.phisical)
            //.vulkan_api_version(vk::make_api_version(0, crate::VULKAN_VER_MAJOR, crate::MIN_VULKAN_VER_MINOR, 0));
            ;
        let aloc = vma::Allocator::new(info)?;

        Ok(Self {
            inner: aloc,
            dev,
        })
    }

    pub fn dev(&self) -> &Device {
        &self.dev
    }
}

/// A Vulkan Buffer, wraps [`vk::Buffer`]
pub struct Buffer {
    allocation: Allocation,
    pub(crate) handle: vk::Buffer,
    alloc: Arc<Allocator>,
    mapped: bool,
    ptr: *mut u8,
}

impl Buffer {
    /// Create Vulkan Buffer
    ///
    /// # Safety
    /// Application is responsible for following the usage specified
    pub unsafe fn new(
        alloc: Arc<Allocator>,
        mem_usage: MemoryUsage,
        buf_usage: vk::BufferUsageFlags,
        size: vk::DeviceSize,
    ) -> VkResult<Self> {
        let buf_info = vk::BufferCreateInfo {
            usage: buf_usage,
            size,
            ..Default::default()
        };

        let alloc_info = vma::AllocationCreateInfo {
            usage: mem_usage,
            ..Default::default()
        };

        let (buf, allocation) = alloc.inner.create_buffer(&buf_info, &alloc_info)?;
        Ok(Self {
            handle: buf,
            allocation,
            alloc,
            mapped: false,
            ptr: null_mut(),
        })
    }

    pub fn map(&mut self) -> VkResult<()> {
        if self.mapped {
            return Ok(())
        }

        let ptr = unsafe { self.alloc.inner.map_memory(&mut self.allocation)? };
        self.ptr = ptr;
        self.mapped = true;

        return Ok(());
    }

    /// Copy data to the buffer
    ///
    /// # Panics
    /// If the buffer is not mapped.
    ///
    /// # Safety
    /// Data must not be bigger than the buffer.
    pub unsafe fn copy_to(&mut self, data: &[u8]) {
        self.copy_to_with_offset(0, data);
    }

    pub unsafe fn copy_to_with_offset(&mut self, off: usize, data: &[u8]) {
        if !self.mapped {
            panic!("Buffer must be mapped");
        }

        ptr::copy_nonoverlapping(data.as_ptr(), self.ptr.add(off), data.len());
    }

    pub fn unmap(&mut self) {
        if !self.mapped {
            return;
        }

        unsafe { self.alloc.inner.unmap_memory(&mut self.allocation) };
        self.ptr = null_mut();
        self.mapped = false;
    }

    #[inline]
    pub fn mapped(&self) -> bool {
        self.mapped
    }

    #[inline]
    pub fn handle(&self) -> vk::Buffer {
        self.handle
    }
}

impl Drop for Buffer {
    fn drop(&mut self) {
        unsafe {
            self.unmap();
            self.alloc.inner.destroy_buffer(self.handle, &mut self.allocation);
        }
    }
}

pub struct Image {
    allocation: Allocation,
    pub(crate) handle: vk::Image,
    alloc: Arc<Allocator>,
    format: vk::Format,
    mip_levels: u32,
    array_layers: u32,
    layout: vk::ImageLayout,
    access_mask: vk::AccessFlags2KHR,
    aspect: vk::ImageAspectFlags,
    extent: vk::Extent3D,
    needs_drop: bool,
}

impl Image {
    /// Create Vulkan Buffer
    ///
    /// # Safety
    /// Application is responsible for following the usage specified
    pub unsafe fn new(
        alloc: Arc<Allocator>,
        mem_usage: MemoryUsage,
        img_usage: vk::ImageUsageFlags,
        format: vk::Format,
        extent: vk::Extent3D,
        image_type: vk::ImageType,
        mip_levels: u32,
        array_layers: u32,
        aspect: vk::ImageAspectFlags,
        flags: Option<vk::ImageCreateFlags>
    ) -> VkResult<Self> {
        let img_info = vk::ImageCreateInfo {
            format,
            usage: img_usage,
            extent,
            mip_levels,
            flags: flags.unwrap_or(vk::ImageCreateFlags::default()),
            image_type,
            array_layers,
            samples: vk::SampleCountFlags::TYPE_1,

            ..Default::default()
        };

        let alloc_info = vma::AllocationCreateInfo {
            usage: mem_usage,
            ..Default::default()
        };

        let (image, allocation) = alloc.inner.create_image(&img_info, &alloc_info)?;

        Ok(Self {
            alloc,
            handle: image,
            allocation,
            format,
            mip_levels,
            array_layers,
            layout: vk::ImageLayout::UNDEFINED,
            access_mask: vk::AccessFlags2KHR::empty(),
            aspect,
            extent,
            needs_drop: true,
        })
    }

    pub fn format(&self) -> vk::Format {
        self.format
    }

    pub unsafe fn cmd_change_layout(
        &mut self,
        buf: &CommandBuffer,
        new_layout: vk::ImageLayout,
        dst_access_mask: vk::AccessFlags2KHR,
        src_stage: vk::PipelineStageFlags2KHR,
        dst_stage: vk::PipelineStageFlags2KHR,
    ) {
        let img_barrier = vk::ImageMemoryBarrier2KHR {
            old_layout: self.layout,
            new_layout,
            src_stage_mask: src_stage,
            dst_stage_mask: dst_stage,
            src_access_mask: self.access_mask,
            dst_access_mask,
            image: self.handle,
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: self.aspect,
                base_mip_level: 0,
                level_count: self.mip_levels,
                base_array_layer: 0,
                layer_count: self.array_layers,
            },
            ..Default::default()
        };

        let info = vk::DependencyInfoKHR {
            image_memory_barrier_count: 1,
            p_image_memory_barriers: &img_barrier,
            ..Default::default()
        };

        buf.cmd_pipeline_barrier2(&info);

        self.layout = new_layout;
        self.access_mask = dst_access_mask;
    }

    pub unsafe fn cmd_gen_mipmaps(
        &mut self,
        buf: &CommandBuffer,
        final_layout: vk::ImageLayout,
        final_access: vk::AccessFlags2KHR,
        final_stage: vk::PipelineStageFlags2KHR,
    ) {
        let mut barrier = vk::ImageMemoryBarrier2KHR {
            image: self.handle,
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: self.aspect,
                base_array_layer: 0,
                layer_count: 1,
                base_mip_level: 0,
                level_count: self.array_layers,
            },
            ..Default::default()
        };

        let barrier_info = vk::DependencyInfo {
            image_memory_barrier_count: 1,
            p_image_memory_barriers: &barrier,
            ..Default::default()
        };


        let mut mip_width = self.extent.width as i32;
        let mut mip_height = self.extent.height as i32;

        for i in 1..self.mip_levels {
            barrier.subresource_range.base_mip_level = i - 1;
            barrier.old_layout = self.layout;
            let new_layout = vk::ImageLayout::TRANSFER_SRC_OPTIMAL;
            barrier.new_layout = new_layout;

            let new_access = vk::AccessFlags2KHR::TRANSFER_READ;
            barrier.src_access_mask = self.access_mask;
            barrier.dst_access_mask = new_access;

            barrier.src_stage_mask = vk::PipelineStageFlags2KHR::TRANSFER;
            barrier.dst_stage_mask = vk::PipelineStageFlags2KHR::TRANSFER;

            buf.cmd_pipeline_barrier2(&barrier_info);

            let blit = vk::ImageBlit {
                src_offsets: [
                    vk::Offset3D { x: 0, y: 0, z: 0 },
                    vk::Offset3D { x: mip_width, y: mip_height, z: 1 },
                ],
                src_subresource: vk::ImageSubresourceLayers {
                    aspect_mask: self.aspect,
                    mip_level: i - 1,
                    base_array_layer: 0,
                    layer_count: self.array_layers,
                },

                dst_offsets: [
                    vk::Offset3D { x: 0, y: 0, z: 0 },
                    vk::Offset3D {
                        x: if mip_width > 1 { mip_width / 2 } else { 1 },
                        y: if mip_height > 1 { mip_height / 2 } else { 1 },
                        z: 1
                    },
                ],
                dst_subresource: vk::ImageSubresourceLayers {
                    aspect_mask: self.aspect,
                    mip_level: i,
                    base_array_layer: 0,
                    layer_count: self.array_layers,
                }
            };

            buf.cmd_blit_image(&self, vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                               &self, vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                               &[blit], vk::Filter::LINEAR);

            barrier.old_layout = vk::ImageLayout::TRANSFER_SRC_OPTIMAL;
            barrier.new_layout = final_layout;
            barrier.src_access_mask = vk::AccessFlags2KHR::TRANSFER_READ;
            barrier.dst_access_mask = final_access;
            barrier.dst_stage_mask = final_stage;

            buf.cmd_pipeline_barrier2(&barrier_info);

            if mip_width > 1 { mip_width /= 2 };
            if mip_height > 1 { mip_height /= 2 };
        }

        // also transfer the layout of the smallist mip level
        barrier.subresource_range.base_mip_level = self.mip_levels - 1;
        barrier.old_layout = vk::ImageLayout::TRANSFER_DST_OPTIMAL;
        barrier.new_layout = final_layout;
        barrier.src_access_mask = vk::AccessFlags2KHR::TRANSFER_WRITE;
        barrier.dst_access_mask = final_access;
        barrier.dst_stage_mask = final_stage;

        buf.cmd_pipeline_barrier2(&barrier_info);

        self.layout = final_layout;
        self.access_mask = final_access;
    }

    pub unsafe fn cmd_copy_to_from_buffer(
        &self,
        cmd_buf: &CommandBuffer,
        buf: &Buffer,
    ) {
        let region = vk::BufferImageCopy {
            buffer_offset: 0,
            buffer_row_length: 0,
            buffer_image_height: 0,

            image_subresource: vk::ImageSubresourceLayers {
                aspect_mask: self.aspect,
                mip_level: 0,
                base_array_layer: 0,
                layer_count: self.array_layers,
            },
            image_extent: self.extent,
            ..Default::default()
        };

        cmd_buf.cmd_copy_buffer_to_image(buf, self, self.layout, &[region]);
    }

    pub fn create_view(&self, ty: vk::ImageViewType, components: vk::ComponentMapping) -> VkResult<ImageView> {
        let info = vk::ImageViewCreateInfo {
            image: self.handle,
            view_type: ty,
            format: self.format,
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: self.aspect,
                base_mip_level: 0,
                level_count: self.mip_levels,
                base_array_layer: 0,
                layer_count: self.array_layers,
            },
            components,
            ..Default::default()
        };
        let handle = unsafe { self.alloc.dev.handle.create_image_view(&info, None)? };
        Ok(ImageView { dev: Arc::clone(&self.alloc.dev), handle, })
    }

    #[inline]
    pub fn mip_levels(&self) -> u32 {
        self.mip_levels
    }

    #[inline]
    pub fn handle(&self) -> vk::Image {
        self.handle
    }

    #[inline]
    pub fn layout(&self) -> vk::ImageLayout {
        self.layout
    }
}

impl Drop for Image {
    fn drop(&mut self) {
        if self.needs_drop {
            unsafe {
                self.alloc.inner.destroy_image(self.handle, &mut self.allocation);
            }
        }
    }
}

pub struct ImageView {
    dev: Arc<Device>,
    handle: vk::ImageView,
}

impl ImageView {
    #[inline]
    pub fn handle(&self) -> vk::ImageView {
        self.handle
    }
}

impl Drop for ImageView {
    fn drop(&mut self) {
        unsafe {
            self.dev.handle.destroy_image_view(self.handle, None);
        }
    }
}

pub struct Sampler {
    dev: Arc<Device>,
    handle: vk::Sampler,
}

impl Sampler {
    pub fn new(
        dev: Arc<Device>,
        filter: vk::Filter,
        address_mode: vk::SamplerAddressMode,
        unormalized_corards: vk::Bool32,
        mipmap_mode: Option<vk::SamplerMipmapMode>,
        mip_levels: Option<u32>,
    ) -> VkResult<Sampler> {
        let info = vk::SamplerCreateInfo {
            mag_filter: filter,
            min_filter: filter,
            address_mode_u: address_mode,
            address_mode_v: address_mode,
            address_mode_w: address_mode,
            anisotropy_enable: vk::FALSE,
            max_anisotropy: 1.0,
            unnormalized_coordinates: unormalized_corards,

            mipmap_mode: mipmap_mode.unwrap_or(vk::SamplerMipmapMode::LINEAR),
            max_lod: mip_levels.unwrap_or(0) as f32,
            ..Default::default()
        };

        let handle = unsafe { dev.handle.create_sampler(&info, None) }?;
        Ok(Self {
            dev, handle
        })
    }

    #[inline]
    pub fn handle(&self) -> vk::Sampler {
        self.handle
    }
}

impl Drop for Sampler {
    fn drop(&mut self) {
        unsafe {
            self.dev.handle.destroy_sampler(self.handle, None);
        }
    }
}

/// calulates the recomended number of mip levels for a 2d image
#[inline]
pub fn calc_mip_levels(wid: u32, height: u32) -> u32 {
    u32::ilog2(u32::max(wid, height))
}

impl_nameable_for_type!(Buffer, vk::ObjectType::BUFFER, alloc);
impl_nameable_for_type!(Image, vk::ObjectType::IMAGE, alloc);
