//! Utilities

use std::{
    sync::Arc,
    slice,
    mem::size_of,
    ffi::c_void,
};

use ash::prelude::*;
use ash::vk;

use crate::{
    CommandPool,
    CommandBuffer,
    Device,
    Queue,
    sync::Fence,
    mem::{Allocator, Buffer, MemoryUsage, Image}
};

/// A utility for uploading data to the gpu
pub struct Uploader {
    fence: Fence,
    pool: CommandPool,
    buffers: Vec<CommandBuffer>,
    transfer_queue: Queue
}

impl Uploader {
    /// Create a new uploader
    ///
    /// Queue must be a queue of the [`QueueType::TransferOnly`] family
    pub fn new(dev: Arc<Device>, queue: Queue) -> VkResult<Self> {
        let fence = Fence::new(Arc::clone(&dev), vk::FenceCreateFlags::empty())?;
        let pool = CommandPool::new(dev, queue.get_type(), vk::CommandPoolCreateFlags::empty())?;
        let buffers = pool.create_command_buffers(1, true)?;

        Ok(Self {
            fence,
            pool,
            buffers,
            transfer_queue: queue,
        })
    }

    pub fn immediate_submit<FUNC>(&self, fun: FUNC) -> VkResult<()>
        where FUNC: FnOnce(&CommandBuffer)
    {
        unsafe {
            self.pool.reset_pool(vk::CommandPoolResetFlags::empty())?;
            let buf = &self.buffers[0];
            buf.begin(true)?;
            {
                fun(buf);
            }
            buf.end()?;
            self.transfer_queue.submit(&[buf], &[], &[], &[], Some(&self.fence))?;
        }
        self.fence.wait_for(u64::MAX)?;
        self.fence.reset()?;

        Ok(())
    }

    /// Upload a image to the gpu
    ///
    /// # Safety
    /// See [`Image::new`]
    pub unsafe fn upload_image(
        &self,
        alloc: Arc<Allocator>,
        data: &[u8],
        format: vk::Format,
        extent: vk::Extent3D,
        ty: vk::ImageType,
        usage: vk::ImageUsageFlags,
        mip_levels: u32,
        aspect: vk::ImageAspectFlags,
        final_layout: vk::ImageLayout,
        final_access_mask: vk::AccessFlags2KHR,
        dst_stage: vk::PipelineStageFlags2KHR,
        flags: Option<vk::ImageCreateFlags>,
    ) -> VkResult<Image> {
        let mut stageing_buffer = Buffer::new(Arc::clone(&alloc)
                                          , MemoryUsage::CpuOnly
                                          , vk::BufferUsageFlags::TRANSFER_SRC
                                          , data.len() as vk::DeviceSize)?;
        stageing_buffer.map()?;
        {
            stageing_buffer.copy_to(data);
        }
        stageing_buffer.unmap();

        let mut usage = usage | vk::ImageUsageFlags::TRANSFER_DST;
        if mip_levels > 1 {
            usage |= vk::ImageUsageFlags::TRANSFER_SRC;
        }

        let mut image = Image::new(
            alloc, MemoryUsage::GpuOnly, usage,
            format, extent, ty, mip_levels, 1, aspect, flags)?;

        self.immediate_submit(|buf| {
            image.cmd_change_layout(buf,
                                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                                vk::AccessFlags2KHR::TRANSFER_WRITE,
                                vk::PipelineStageFlags2KHR::NONE,
                                vk::PipelineStageFlags2KHR::TRANSFER);

            image.cmd_copy_to_from_buffer(buf, &stageing_buffer);

            if mip_levels > 1 {
                image.cmd_gen_mipmaps(buf, final_layout, final_access_mask, dst_stage);
            } else {
                image.cmd_change_layout(buf, final_layout, final_access_mask, vk::PipelineStageFlags2KHR::TRANSFER, dst_stage);
            }
        })?;

        Ok(image)
    }

    /// An opinionated wrapper over [`Self::upload_image`] for uploadeing textures
    pub unsafe fn upload_texture(
        &self,
        alloc: Arc<Allocator>,
        data: &[u8],
        format: vk::Format,
        width: u32,
        height: u32,
        gen_mip_maps: bool,
    ) -> VkResult<Image> {
        let extent = vk::Extent3D {
            width,
            height,
            depth: 1,
        };
        let mip_levels = if gen_mip_maps { crate::mem::calc_mip_levels(width, height) } else { 1 };
        self.upload_image(
            alloc, data, format, extent, vk::ImageType::TYPE_2D, vk::ImageUsageFlags::SAMPLED,
            mip_levels, vk::ImageAspectFlags::COLOR,
            vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL, vk::AccessFlags2KHR::SHADER_READ,
            vk::PipelineStageFlags2KHR::FRAGMENT_SHADER, None)
    }
}

#[inline]
pub fn to_byte_slice<T>(src: &[T]) -> &[u8] {
    unsafe { slice::from_raw_parts(src.as_ptr() as *const u8, src.len() * size_of::<T>()) }
}

pub fn pad_uniform_buffer_size(min_align: vk::DeviceSize, size: u64) -> vk::DeviceSize {
    // based off https://github.com/SaschaWillems/Vulkan/blob/master/examples/dynamicuniformbuffer/dynamicuniformbuffer.cpp#L397
    if min_align > 0 {
        (size + min_align - 1) & !(min_align - 1)
    } else {
        size
    }
}

#[inline]
pub(crate) fn to_void<T>(r: &T) -> *const c_void {
    r as *const T as *const c_void
}

#[inline]
pub(crate) fn to_void_mut<T>(r: &mut T) -> *mut c_void {
    r as *mut T as *mut c_void
}

/// Create a `*const c_char` from a `&'static str`.
#[macro_export]
macro_rules! cstr {
    ($string:expr) => {
        concat!($string, "\0").as_ptr() as *const ::std::os::raw::c_char
    };
}

/// Create a rust [`CStr`](std::ffi::CStr) from a `&'static str`.
#[macro_export]
macro_rules! rcstr {
    ($string:expr) => {
        unsafe {
            ::std::ffi::CStr::from_bytes_with_nul_unchecked(concat!($string, "\0").as_bytes())
        }
    };
}

/// Caculate the offset of a struct field
#[macro_export]
macro_rules! offset_of {
    ($struct:ty, $field:tt) => {
        unsafe {
            let dummy = ::core::mem::zeroed::<$struct>();
            let dummy_ptr = (&dummy) as *const _;
            let member_ptr = (&dummy.$field) as *const _;
            let offset = member_ptr as usize - dummy_ptr as usize;
            offset
        }
    };
}
