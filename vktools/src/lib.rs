#![allow(clippy::needless_return)]
#![allow(clippy::into_iter_on_ref)]
//! Wraps various low-level vulkan things
//!
//! ## Note on safety
//! A decent attempt is made to only allow valid actions with safe functions but
//! this crate does not 100% guarantee that safe functions will not cause validation
//! errors; it is recommended to enable the validation layers during development.
//! This crate considers it safe to retrieve the underlying vulkan handle from a wrapped handle
//! becuase it is impossible to use it without `unsafe`. These handles should never
//! be freed and must be used according to the parameters specified when they are created.


#[cfg(not(any(feature = "loaded-vulkan", feature = "linked-vulkan")))]
compile_error!("vktools: EXACTLY ONE of the following features must be enabled `linked-vulkan`, `loaded-vulkan`");

#[cfg(all(feature = "loaded-vulkan", feature = "linked-vulkan"))]
compile_error!("vktools: EXACTLY ONE of the following features must be enabled `linked-vulkan`, `loaded-vulkan`");

pub use ash;

pub mod mem;
pub mod pipeline;
pub mod sync;
pub mod util;

use std::{
    collections::HashSet,
    default::Default,
    ffi::{CStr, CString, c_void},
    os::raw::c_char,
    ptr::{null, null_mut},
    sync::Arc,
};

use anyhow::{
    anyhow, bail
};

use ash::prelude::*;
use ash::vk;

use raw_window_handle::HasRawWindowHandle;

use crate::mem::{Buffer, Image};
use crate::util::{to_void, to_void_mut};

const VALIDATION_LAYERS_NAME: &CStr = rcstr!("VK_LAYER_KHRONOS_validation");

const VULKAN_VER_MAJOR: u32 = 1;
const MIN_VULKAN_VER_MINOR: u32 = 1;

/// Sets up and wraps [`ash::Instance`]
pub struct Instance {
    instance: ash::Instance,
    debug_utils_ext: Option<ash::extensions::ext::DebugUtils>,
    debug_messenger: Option<vk::DebugUtilsMessengerEXT>,
    entry: ash::Entry,
    surface_khr: ash::extensions::khr::Surface,
    surface: vk::SurfaceKHR,
    _window_handle: Arc<dyn HasRawWindowHandle>,
}

impl Instance {
    /// Initialize vulkan, includes creating a vulkan 1.1 instance, surface, and debug messenger.
    ///
    /// Use [`vk::make_api_version`] to fill the `app_version` parameter.
    pub fn new(app_name: &'static CStr, app_version: u32
               , window: Arc<dyn HasRawWindowHandle>) -> anyhow::Result<Self>
    {
        #[cfg(feature = "loaded-vulkan")]
        let entry = unsafe { ash::Entry::load()? };

        #[cfg(feature = "linked-vulkan")]
        let entry = ash::Entry::linked();

        let ver = entry.try_enumerate_instance_version()?.ok_or_else(|| anyhow!("failed to get vulkan version"))?;
        if vk::api_version_variant(ver) != 0 {
            bail!("unsuported vulkan version");
        }
        if vk::api_version_major(ver) != VULKAN_VER_MAJOR {
            bail!("unsuported vulkan version");
        }
        if vk::api_version_minor(ver) < MIN_VULKAN_VER_MINOR {
            bail!("vulkan version too old");
        }

        // create instance

        let app_info = vk::ApplicationInfo {
            p_application_name: app_name.as_ptr(),
            p_engine_name: cstr!("vktools"),
            api_version: vk::make_api_version(0, VULKAN_VER_MAJOR, MIN_VULKAN_VER_MINOR, 0),
            engine_version: vk::make_api_version(0, 0, 1, 0),
            application_version: app_version,
            ..Default::default()
        };

        let mut req_ext = ash_window::enumerate_required_extensions(window.as_ref())?.to_vec();

        let mut wants_layers = Vec::new();

        if cfg!(feature = "vkdebug") {
            wants_layers.push(VALIDATION_LAYERS_NAME);
            req_ext.push(ash::extensions::ext::DebugUtils::name().as_ptr());
        }

        if !has_layers(&wants_layers, &entry)? {
            bail!("failed to find one ore more vulkan layers")
        }

        let wants_layers: Vec<*const c_char> = wants_layers.into_iter().map(|str| str.as_ptr()).collect();

        let instance_info = vk::InstanceCreateInfo {
            p_application_info: &app_info,

            enabled_extension_count: req_ext.len() as u32,
            pp_enabled_extension_names: req_ext.as_ptr(),

            enabled_layer_count: wants_layers.len() as u32,
            pp_enabled_layer_names: wants_layers.as_ptr(),
            ..Default::default()
        };

        // SAFETY: All inputs to create_instance are ether &'static or refrences that will be valid for the
        //         lifetime of Instance.
        // CLEANUP: Drop
        let instance = unsafe { entry.create_instance(&instance_info, None)? };

        // create debug messenger

        let mut debug_utils = None;
        let mut debug_messenger = None;
        if cfg!(feature = "vkdebug") {
            log::info!("Enabling debug utils vulkan extention");
            debug_utils = Some(ash::extensions::ext::DebugUtils::new(&entry, &instance));

            let debug_messager_info = vk::DebugUtilsMessengerCreateInfoEXT {
                message_severity:
                    vk::DebugUtilsMessageSeverityFlagsEXT::ERROR
                    | vk::DebugUtilsMessageSeverityFlagsEXT::WARNING,
                message_type:
                    vk::DebugUtilsMessageTypeFlagsEXT::GENERAL
                    | vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION
                    | vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE,
                pfn_user_callback: Some(debug_messenger_callback),
                p_user_data: null_mut(),
                ..Default::default()
            };

            // SAFETY: all values in debug_messager_info are &'static
            // CLEANUP: Drop
            debug_messenger = unsafe {
                Some(debug_utils.as_ref().unwrap()
                        .create_debug_utils_messenger(&debug_messager_info, None)?)
            }
        }

        // create surface

        let surface_khr = ash::extensions::khr::Surface::new(&entry, &instance);
        // SAFETY: instance and entry created above and destoryed after in Drop
        // CLEANUP: Drop
        let surface = unsafe { ash_window::create_surface(&entry, &instance, window.as_ref(), None)? };

        Ok(Self {
            entry, instance, debug_utils_ext: debug_utils, debug_messenger, surface_khr, surface
            , _window_handle: window
        })
    }

    /// Get the internal [`ash::Instance`].
    #[inline]
    pub fn handle(&self) -> &ash::Instance {
        &self.instance
    }

    #[inline]
    /// gets the internal [`ash::Entry`]
    pub fn ash_entry(&self) -> &ash::Entry {
        &self.entry
    }
}

impl Drop for Instance {
    fn drop(&mut self) {
        unsafe { // SAFETY: cleanup
            log::info!("Destorying instance");
            self.surface_khr.destroy_surface(self.surface, None);

            if self.debug_messenger.is_some() {
                assert!(self.debug_utils_ext.is_some());
                let debug_utils = self.debug_utils_ext.as_ref().unwrap();
                debug_utils.destroy_debug_utils_messenger(self.debug_messenger.unwrap(), None);
            }

            self.instance.destroy_instance(None);
        }
    }
}

fn has_layers(wants: &[&CStr], entry: &ash::Entry) -> VkResult<bool> {
    let mut layer_names: Vec<CString> = Vec::new();
    for prop in entry.enumerate_instance_layer_properties()? {
        // SAFETY: the vulkan spec guarantees prop.layer_name is a valid utf8 c string
        unsafe {
            layer_names.push(CStr::from_ptr(prop.layer_name.as_ptr()).to_owned());
        }
    }

    for want in wants.iter() {
        let mut has_layer = false;
        for layer in &layer_names {
            if *want == layer.as_c_str() {
                has_layer = true;
                break;
            }
        }

        if !has_layer {
            log::error!("does not have layer `{}`", want.to_string_lossy());
            return Ok(false);
        }
    }

    return Ok(true);
}

#[derive(Clone)]
struct QueueFamilyIndecies {
                /*  index, count */
    grphics: Option<(u32, u32)>,
    compute_only: Option<(u32, u32)>,
    transfer_only: Option<(u32, u32)>,
}

#[derive(Clone)]
struct SwapchainSupportDetails {
    capabilities: vk::SurfaceCapabilitiesKHR,
    formats: Vec<vk::SurfaceFormatKHR>,
    present_modes: Vec<vk::PresentModeKHR>,
}

impl QueueFamilyIndecies {
                                            /* index, quantity */
    fn get_index_for_type(&self, ty: QueueType) -> Option<(u32, u32)> {
        use QueueType::*;
        match ty {
            Graphics => self.grphics,
            ComputeOnly => self.compute_only,
            TransferOnly => self.transfer_only,
        }
    }
}

// TODO: permit application to request extentions
const REQUIRED_DEVICE_EXTENTIONS: [&CStr; 6]= [
    ash::extensions::khr::Swapchain::name(),
    ash::extensions::khr::Synchronization2::name(),

    ash::extensions::khr::DynamicRendering::name(),
    // deps for dynamic rendering
    ash::extensions::khr::CreateRenderPass2::name(),
    rcstr!("VK_KHR_depth_stencil_resolve"), // adds no new function pointers

    rcstr!("VK_EXT_scalar_block_layout"),

    // debug printf
    // rcstr!("VK_KHR_shader_non_semantic_info"),
];

// TODO: un-hardcode this
const DESIRED_SURFACE_FORMAT: vk::SurfaceFormatKHR = vk::SurfaceFormatKHR {
        format: vk::Format::R8G8B8_SRGB,
        color_space: vk::ColorSpaceKHR::SRGB_NONLINEAR,
};

/// Represents a GPU in the computer, wraps [`vk::PhysicalDevice`].
#[derive(Clone)]
pub struct PhysicalDevice {
    ash_physical_device: vk::PhysicalDevice,
    queue_families: QueueFamilyIndecies,
    // I am using a reference instead of PhantomData because there will likely be
    // methods that utilize this in the near future
    instance: Arc<Instance>,
}

impl PhysicalDevice {
    /// pick an approved physical device
    pub fn pick(
        instance: Arc<Instance>,
        reqired_format_features: &[(vk::Format, vk::FormatProperties)],
    ) -> anyhow::Result<Self> {
        #[inline]
        fn queue_wants(queue: vk::QueueFlags, wants: vk::QueueFlags, not: vk::QueueFlags) -> bool {
           (queue & wants) != vk::QueueFlags::empty() && (queue & not) == vk::QueueFlags::empty()
        }

        fn has_extentions(instance: &ash::Instance, dev: vk::PhysicalDevice
                          , mut wants: HashSet<&CStr>, name: &str) -> bool {
                                        // NOTE: will pacnic on oom, see my note below
            let has: Vec<_> = unsafe { instance.enumerate_device_extension_properties(dev).unwrap() }
                .iter().map(|prop| unsafe { CStr::from_ptr(prop.extension_name.as_ptr()).to_owned() })
                .collect();

            for extention in has {
                wants.remove(extention.as_c_str());
            }

            if wants.is_empty() {
                return true;
            } else {
                log::warn!("Device `{name}` is not suitable due to lacking the folowing extentions: {:?}", wants);
                return false;
            }
        }
        // returns None if the device is not valid
        let get_dev_score = |device: vk::PhysicalDevice| -> Option<(u64, vk::PhysicalDevice)> {

            let ainstance = instance.handle();
            let mut score = 0;
            // SAFETY: instance is always valid
            unsafe {
                let prop = ainstance.get_physical_device_properties(device);
                let name = CStr::from_ptr(prop.device_name.as_ptr()).to_str().unwrap();
                if vk::api_version_variant(prop.api_version) != 0
                        || vk::api_version_major(prop.api_version) != VULKAN_VER_MAJOR
                        || vk::api_version_minor(prop.api_version) < MIN_VULKAN_VER_MINOR {

                    log::warn!("Device `{name}` is not suitable due to not supporting a version of vulkan that is semver compatible with 1.1");
                    return None;
                }

                let queues = ainstance.get_physical_device_queue_family_properties(device);

                // log::debug!("testing device with name {}", CStr::from_ptr(prop.device_name.as_ptr()).to_str().unwrap());

                let extentions = REQUIRED_DEVICE_EXTENTIONS.into();
                if !has_extentions(ainstance, device, extentions, name) {
                    return None;
                }

                let mut scalar_layout_features = vk::PhysicalDeviceScalarBlockLayoutFeaturesEXT {
                    ..Default::default()
                };

                let mut sync2_features = vk::PhysicalDeviceSynchronization2FeaturesKHR {
                    p_next: to_void_mut(&mut scalar_layout_features),
                    ..Default::default()
                };

                let mut dynamic_rendering_features = vk::PhysicalDeviceDynamicRenderingFeaturesKHR {
                    p_next: to_void_mut(&mut sync2_features),
                    ..Default::default()
                };

                let mut features2  = vk::PhysicalDeviceFeatures2 {
                    p_next: to_void_mut(&mut dynamic_rendering_features),
                    ..Default::default()
                };

                ainstance.get_physical_device_features2(device, &mut features2);

                if sync2_features.synchronization2 == vk::FALSE {
                    log::warn!("Device `{name}` is not suitable due to lacking the synchronization2 feature");
                    return None;
                }

                if dynamic_rendering_features.dynamic_rendering == vk::FALSE {
                    log::warn!("Device `{name}` is not suitable due to lacking the dynamic_rendering feature");
                    return None;
                }

                if scalar_layout_features.scalar_block_layout == vk::FALSE {
                    log::warn!("Device `{name}` is not suitable due to lacking the std430 layout for uniforms");
                    return None;
                }

                    // NOTE: will pacnic on oom, see my note below
                let surface_formats = instance.surface_khr
                        .get_physical_device_surface_formats(device, instance.surface).unwrap();
                    // NOTE: will pacnic on oom, see my note below
                let surface_present_modes = instance.surface_khr
                        .get_physical_device_surface_present_modes(device, instance.surface).unwrap();

                if surface_formats.is_empty() || surface_present_modes.is_empty() {
                    log::warn!("Device `{name}` is not suitable due to haveing no surface formats or present modes");
                    return None;
                }

                if surface_formats.contains(&DESIRED_SURFACE_FORMAT) {
                    score += 10;
                }

                if surface_present_modes.contains(&vk::PresentModeKHR::MAILBOX) {
                    score += 10;
                }

                if prop.device_type == vk::PhysicalDeviceType::DISCRETE_GPU {
                    score += 100;
                }

                let mut has_graphics_queue = false;
                for (i, queue) in queues.into_iter().enumerate() {
                    // queue suports transfer but not graphics or compute
                    if queue_wants(
                            queue.queue_flags,
                            vk::QueueFlags::TRANSFER,
                            vk::QueueFlags::COMPUTE | vk::QueueFlags::GRAPHICS
                            ) && queue.queue_count > 0 {
                        score += 10;
                    }

                    if (queue.queue_flags & vk::QueueFlags::GRAPHICS) != vk::QueueFlags::empty() {
                        let present_support = instance.surface_khr
                            .get_physical_device_surface_support(device, i as u32, instance.surface)
                            // NOTE: techinicaly this can oom but the rust std panics on oom so it is not the
                            // end of the world
                            .expect("Failed to get surface support");
                        has_graphics_queue = present_support;
                    }

                }

                if !has_graphics_queue {
                    log::warn!("Device `{name}` is not suitable due to lacking graphics queues");
                    return None;
                }

                for format in reqired_format_features {
                    let format_prop = ainstance.get_physical_device_format_properties(device, format.0);
                    if (format_prop.buffer_features & format.1.buffer_features)
                            != format.1.buffer_features
                        || (format_prop.optimal_tiling_features & format.1.optimal_tiling_features)
                            != format.1.optimal_tiling_features
                        || (format_prop.linear_tiling_features & format.1.linear_tiling_features)
                            != format.1.linear_tiling_features {
                        log::warn!("Device `{name}` is not suitable due to lacking features for `{:?}`
            has: {:?}
            wants: {:?}",
            format.0, format_prop, format.1);
                        return None;
                    }
                }
            }
            Some((score, device))
        };

        // SAFETY: instance is always valid
        let (_, device) = unsafe { instance.handle().enumerate_physical_devices()? }.iter()
            .filter_map(|dev| get_dev_score(*dev))
            .max_by_key(|d| d.0)
            .ok_or_else(|| anyhow!("No suitable GPU"))?;

        let queues = unsafe { instance.handle().get_physical_device_queue_family_properties(device) };
        let mut queue_families = QueueFamilyIndecies { grphics: None, compute_only: None, transfer_only: None  };
        // TODO: detect dedicated and seperate queues
        for (i, queue) in queues.into_iter().enumerate() {
            if (queue.queue_flags & vk::QueueFlags::GRAPHICS) != vk::QueueFlags::empty() {
                queue_families.grphics = Some((i as u32, queue.queue_count));
            }
            if queue_wants(queue.queue_flags, vk::QueueFlags::TRANSFER
                              , vk::QueueFlags::GRAPHICS | vk::QueueFlags::COMPUTE) {
                queue_families.transfer_only = Some((i as u32, queue.queue_count));
            }
            if queue_wants(queue.queue_flags, vk::QueueFlags::COMPUTE, vk::QueueFlags::GRAPHICS) {
                queue_families.compute_only = Some((i as u32, queue.queue_count));
            }
        }

        if queue_families.grphics.is_some() {
            let count = queue_families.grphics.unwrap().1;
            log::debug!("Selected physical device has {count} graphics capable queues");
        }
        if queue_families.transfer_only.is_some() {
            let count = queue_families.transfer_only.unwrap().1;
            log::debug!("Selected physical device has {count} transfer only queues");
        }
        if queue_families.compute_only.is_some() {
            let count = queue_families.compute_only.unwrap().1;
            log::debug!("Selected physical device has {count} compute only queues");
        }


        Ok(Self { ash_physical_device: device, queue_families, instance })
    }

    pub fn grphics_queue_count(&self) -> Option<u32> {
        self.queue_families.grphics.map(|q| q.1)
    }

    pub fn transfer_only_queue_count(&self) -> Option<u32> {
        self.queue_families.transfer_only.map(|q| q.1)
    }

    pub fn compute_only_queue_count(&self) -> Option<u32> {
        self.queue_families.compute_only.map(|q| q.1)
    }

    /// start building a logical device from this physical device
    pub fn build_device(&self) -> DeviceBuilder {
        DeviceBuilder {
            intance: Arc::clone(&self.instance),
            phy_dev: self,
            queues: Vec::new()
        }
    }
}

/// Types of [`Queue`]s that this library supports.
#[non_exhaustive]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum QueueType {
    /// implies present support
    Graphics,
    ComputeOnly,
    TransferOnly,
}

pub struct DeviceBuilder<'b> {
    intance: Arc<Instance>,
    phy_dev: &'b PhysicalDevice,
    queues: Vec<(QueueType, u32, &'b [f32])>,
    //queue_priorities: Vec<[f32]>,
}

impl<'b> DeviceBuilder<'b> {

    /// Adds a queue to the device being constructed.
    ///
    /// # Panics
    /// Will panic in [`DeviceBuilder::build`] if there is not a valid queue type on the physical device
    ///
    /// # Safety
    /// It is the callers responsibility to not exceed the queue cap and to not request the same.
    /// queue type twice. In addition everything within the prios slice must be normalized See
    ///  the vulkan spec for more info:
    /// <https://www.khronos.org/registry/vulkan/specs/1.3/html/chap5.html#VkDeviceCreateInfo>
    pub unsafe fn request_queue(mut self, ty: QueueType, count: u32, prios: &'b [f32]) -> Self {
        assert!(count <= self.phy_dev.queue_families.get_index_for_type(ty).unwrap().1, "count too big");
        self.queues.push((ty, count, prios));
        //self.queue_priorities.push(prios);
        self
    }

    /// Build logical device.
    ///
    /// # Safety
    /// This function is safe to call if all other safety warnings on [`DeviceBuilder`]
    /// have been followed.
    pub unsafe fn build(self) -> VkResult<Device> {
        let queue_infos = self.queues.iter().map(|q| {
            let family = self.phy_dev.queue_families.get_index_for_type(q.0).unwrap();

            vk::DeviceQueueCreateInfo {
                queue_family_index: family.0,
                queue_count: q.1,
                // ptr should have the lifetime of the builder
                p_queue_priorities: q.2.as_ptr(),
                ..Default::default()
            }
        }).collect::<Vec<vk::DeviceQueueCreateInfo>>();

        let features = vk::PhysicalDeviceFeatures::default();

        let mut scalar_layout_features = vk::PhysicalDeviceScalarBlockLayoutFeaturesEXT {
            scalar_block_layout: vk::TRUE,
            ..Default::default()
        };

        let mut sync2_features = vk::PhysicalDeviceSynchronization2FeaturesKHR {
            p_next: to_void_mut(&mut scalar_layout_features),
            synchronization2: vk::TRUE,
            ..Default::default()
        };

        let dynamic_rendering_feature = vk::PhysicalDeviceDynamicRenderingFeaturesKHR {
            p_next: to_void_mut(&mut sync2_features),
            dynamic_rendering: vk::TRUE,
            ..Default::default()
        };

        let dev_info = vk::DeviceCreateInfo {
            p_next: to_void(&dynamic_rendering_feature),

            p_queue_create_infos: queue_infos.as_ptr(),
            queue_create_info_count: queue_infos.len() as u32,

            enabled_extension_count: REQUIRED_DEVICE_EXTENTIONS.len() as u32,
            pp_enabled_extension_names: REQUIRED_DEVICE_EXTENTIONS.map(|s| s.as_ptr()).as_ptr(),

            p_enabled_features: &features,
            ..Default::default()
        };

        let dev = self.intance.handle().create_device(
            self.phy_dev.ash_physical_device, &dev_info, None)?;

        let swapchain_khr = ash::extensions::khr::Swapchain::new(self.intance.handle(), &dev);
        let dynamic_rendering_khr = ash::extensions::khr::DynamicRendering::new(self.intance.handle(), &dev);
        let sync2_khr = ash::extensions::khr::Synchronization2::new(self.intance.handle(), &dev);

        let prop = self.intance.handle().get_physical_device_properties(self.phy_dev.ash_physical_device);

        Ok(Device {
            handle: dev,
            phisical: self.phy_dev.ash_physical_device,
            prop,
            queue_families: self.phy_dev.queue_families.clone(),
            swapchain_khr,
            dynamic_rendering_khr,
            sync2_khr,
            surface: self.intance.surface,
            instance: self.intance,
        })
    }
}

/// Vulkan logical device, wraps [`ash::Device`]
pub struct Device {
    handle: ash::Device,
    phisical: vk::PhysicalDevice,

    prop: vk::PhysicalDeviceProperties,

    queue_families: QueueFamilyIndecies,

    surface: vk::SurfaceKHR,

    swapchain_khr: ash::extensions::khr::Swapchain,
    dynamic_rendering_khr: ash::extensions::khr::DynamicRendering,
    sync2_khr: ash::extensions::khr::Synchronization2,

    // instance should not be destroyed until device is dead
    #[allow(unused)] // rust thinks this is unused when the vkdebug feature is not enabled
    instance: Arc<Instance>,
}

impl Device {
    #[inline]
    pub fn handle(&self) -> &ash::Device {
        &self.handle
    }

    #[inline]
    pub fn sync2_khr(&self) -> &ash::extensions::khr::Synchronization2 {
        &self.sync2_khr
    }

    pub fn properties(&self) -> &vk::PhysicalDeviceProperties {
        &self.prop
    }

    pub fn wait_idle(&self) -> VkResult<()> {
        unsafe {
            self.handle.device_wait_idle()
        }
    }

    fn swapchain_support(&self) -> VkResult<SwapchainSupportDetails> {
        unsafe {
            let surface_cap = self.instance.surface_khr
                    .get_physical_device_surface_capabilities(self.phisical, self.instance.surface)?;
            let surface_formats = self.instance.surface_khr
                    .get_physical_device_surface_formats(self.phisical, self.instance.surface)?;
            let surface_present_modes = self.instance.surface_khr
                    .get_physical_device_surface_present_modes(self.phisical, self.instance.surface)?;


            Ok(SwapchainSupportDetails {
                capabilities: surface_cap,
                formats: surface_formats,
                present_modes: surface_present_modes,
            })
        }

    }
}

impl Drop for Device {
    fn drop(&mut self) {
        unsafe {
            self.handle.destroy_device(None);
        }
    }
}

/// Begin rendering useing the dynamic rendering extention
pub fn begin_rendering(
    dev: &Device,
    buf: &CommandBuffer,
    image_view: vk::ImageView,
    clear_value: vk::ClearValue,
    render_area: vk::Rect2D,
) {
    let color_attachment = vk::RenderingAttachmentInfoKHR {
        image_view,
        clear_value,
        image_layout: vk::ImageLayout::ATTACHMENT_OPTIMAL_KHR,
        load_op: vk::AttachmentLoadOp::CLEAR,
        store_op: vk::AttachmentStoreOp::STORE,
        ..Default::default()
    };

    let info = vk::RenderingInfoKHR {
        render_area,
        layer_count: 1,
        color_attachment_count: 1,
        p_color_attachments: &color_attachment,
        ..Default::default()
    };

    unsafe {
        dev.dynamic_rendering_khr.cmd_begin_rendering(buf.handle, &info);
    }
}

/// Opposite of [`begin_rendering`]
pub fn end_rendering(dev: &Device, buf: &CommandBuffer) {
    unsafe {
        dev.dynamic_rendering_khr.cmd_end_rendering(buf.handle);
    }
}

/// Wraps a [`vk::SwapchainKHR`].
pub struct Swapchain {
    handle: vk::SwapchainKHR,
    format: vk::SurfaceFormatKHR,
    extent: vk::Extent2D,

    // TODO: use image view and image abstration
    images: Vec<vk::Image>,
    image_views: Vec<vk::ImageView>,

    dev: Arc<Device>,
}

impl Swapchain {

    /// Creates a swapchain with the given extent
    pub fn new(dev: Arc<Device>, extent: vk::Extent2D) -> VkResult<Swapchain> {
        let (handle, format, images, image_views) = Self::create_swapchain_impl(&dev, extent, None)?;

        Ok(Self {
            handle,
            format,
            extent,
            images,
            image_views,
            dev,
        })
    }

    pub fn recreate(&mut self, extent: vk::Extent2D) -> VkResult<()> {
        self.dev.wait_idle()?;

        let old_format = self.format;

        let (handle, format, images, image_views) = Self::create_swapchain_impl(&self.dev, extent, Some(self.handle))?;

        if old_format != format {
            log::error!("Old swapchain format differs from new fromat");
            return Err(vk::Result::ERROR_FORMAT_NOT_SUPPORTED);
        }

        unsafe {
            for view in &self.image_views {
                self.dev.handle.destroy_image_view(*view, None);
            }
            self.dev.swapchain_khr.destroy_swapchain(self.handle, None);
        }

        self.handle = handle;
        self.format = format;
        self.images = images;
        self.image_views = image_views;
        self.extent = extent;

        Ok(())
    }

    fn create_swapchain_impl(dev: &Device, extent: vk::Extent2D, old_swapchain: Option<vk::SwapchainKHR>)
     -> VkResult<(vk::SwapchainKHR, vk::SurfaceFormatKHR, Vec<vk::Image>, Vec<vk::ImageView>)> {
        // NOTE: This has to be called here otherwiser you will get cryptic validation errors
        // about ImageView sizes
        let supports = dev.swapchain_support()?;

        let swapchain_khr = &dev.swapchain_khr;
        let surface = dev.surface;

        let present_mode = if supports.present_modes.contains(&vk::PresentModeKHR::MAILBOX) {
            vk::PresentModeKHR::MAILBOX
        } else {
            vk::PresentModeKHR::FIFO
        };

        let surface_format = if supports.formats.contains(&DESIRED_SURFACE_FORMAT) {
            DESIRED_SURFACE_FORMAT
        } else {
            supports.formats[0]
        };

        let min_img_exnt = supports.capabilities.min_image_extent;
        let max_img_exnt = supports.capabilities.max_image_extent;

        let extent = vk::Extent2D {
            width: u32::clamp(extent.width, min_img_exnt.width, max_img_exnt.width),
            height: u32::clamp(extent.width, min_img_exnt.height, max_img_exnt.height),
        };

        let mut image_count = supports.capabilities.min_image_count + 1;
        if supports.capabilities.max_image_count > 0
            && image_count > supports.capabilities.max_image_count
        {
            image_count = supports.capabilities.max_image_count;
        }

        let swapchain_info = vk::SwapchainCreateInfoKHR {
            min_image_count: image_count,
            surface,

            image_extent: extent,
            image_format: surface_format.format,
            image_color_space: surface_format.color_space,
            image_array_layers: 1,
            image_usage: vk::ImageUsageFlags::COLOR_ATTACHMENT,

            image_sharing_mode: vk::SharingMode::EXCLUSIVE,
            queue_family_index_count: 0,
            p_queue_family_indices: null(),

            pre_transform: supports.capabilities.current_transform,
            composite_alpha: vk::CompositeAlphaFlagsKHR::OPAQUE,

            present_mode,
            clipped: vk::TRUE,

            //old_swapchain: vk::SwapchainKHR::null(),
            old_swapchain: old_swapchain.unwrap_or(vk::SwapchainKHR::null()),
            ..Default::default()
        };
        let handle = unsafe { swapchain_khr.create_swapchain(&swapchain_info, None)? };

        let images = unsafe { swapchain_khr.get_swapchain_images(handle)? };
        let image_views = images.iter().map(|image| {
            let info = vk::ImageViewCreateInfo {
                image: *image,

                view_type: vk::ImageViewType::TYPE_2D,
                format: surface_format.format,

                components: vk::ComponentMapping {
                    r: vk::ComponentSwizzle::IDENTITY,
                    g: vk::ComponentSwizzle::IDENTITY,
                    b: vk::ComponentSwizzle::IDENTITY,
                    a: vk::ComponentSwizzle::IDENTITY,
                },

                subresource_range: vk::ImageSubresourceRange {
                    aspect_mask: vk::ImageAspectFlags::COLOR,
                    base_mip_level: 0,
                    level_count: 1,
                    base_array_layer: 0,
                    layer_count: 1,
                },

                ..Default::default()
            };

            unsafe { dev.handle.create_image_view(&info, None) }
        }).collect::<Result<Vec<_>, _>>()?;

        Ok((handle, surface_format, images, image_views))
    }

    pub fn image_format(&self) -> vk::Format {
        self.format.format
    }

    pub fn image_view_with_index(&self, index: usize) -> vk::ImageView {
        self.image_views[index]
    }

    pub fn image_with_index(&self, index: usize) -> vk::Image {
        self.images[index]
    }

    /// Wraps `vkAcquireNextImageKHR`:
    /// <https://www.khronos.org/registry/vulkan/specs/1.3-extensions/man/html/vkAcquireNextImageKHR.html>
    ///
    /// returns the index of the next image and true if the swapchain is suboptimal
    pub fn aquire_next_image_index(
        &self,
        timeout: u64,
        semaphore: Option<&sync::Semaphore>,
        fence: Option<&sync::Fence>,
    ) -> VkResult<(u32, bool)> {
        if semaphore.is_none() && fence.is_none() {
            panic!("the spec states semaphore and fence can't both be None");
        }

        unsafe {
            self.dev.swapchain_khr.acquire_next_image(
                self.handle,
                timeout,
                if let Some(sem) = semaphore { sem.handle } else { vk::Semaphore::null() },
                if let Some(fence) = fence { fence.handle } else { vk::Fence::null() },
            )
        }
    }

    #[inline]
    pub fn extent(&self) -> vk::Extent2D {
        self.extent
    }
}

impl Drop for Swapchain {
    fn drop(&mut self) {
        unsafe {
            for view in &self.image_views {
                self.dev.handle.destroy_image_view(*view, None);
            }
            self.dev.swapchain_khr.destroy_swapchain(self.handle, None);
        }
    }
}

/// A work queue for the GPU, wraps [`vk::Queue`]
pub struct Queue {
    ty: QueueType,
    dev: Arc<Device>,
    handle: vk::Queue,
}

impl Queue {
    /// Retrieve the queues with the type `ty` and `index`.
    ///
    /// See the man page for more details:
    /// <https://www.khronos.org/registry/vulkan/specs/1.3-extensions/man/html/vkGetDeviceQueue.html>
    ///
    /// # Safety
    /// Safe to call if device was created with queues of the provided [`QueueType`]
    /// and the index is in range.
    pub unsafe fn get_from_device(dev: Arc<Device>, ty: QueueType, index: u32) -> Queue {
        let family_idx = dev.queue_families.get_index_for_type(ty).unwrap().0;
        let queue = dev.handle.get_device_queue(family_idx, index);
        Queue { ty, dev, handle: queue }
    }

    #[inline]
    pub fn get_type(&self) -> QueueType {
        self.ty
    }

    /// see the docs for `vkQueueSubmit`:
    /// <https://www.khronos.org/registry/vulkan/specs/1.3-extensions/man/html/vkQueueSubmit.html>
    ///
    /// # Safety
    /// See the spec
    pub unsafe fn submit(
        &self,
        buffers: &[&CommandBuffer],
        wait_semaphores: &[&sync::Semaphore],
        wait_dst_stage_mask: &[vk::PipelineStageFlags],
        signal_semaphores: &[&sync::Semaphore],
        fence: Option<&sync::Fence>,
    ) -> VkResult<()> {
        // NOTE: There probably should be a way to avoid the allocation here but submit is not called often enough
        //       for it to be a big deal.
        let buffer_handles = buffers.into_iter().map(|buf| buf.handle).collect::<Vec<vk::CommandBuffer>>();
        let wait_sem_handles = wait_semaphores.into_iter().map(|sem| sem.handle).collect::<Vec<vk::Semaphore>>();
        let signal_sem_handles = signal_semaphores.into_iter().map(|sem| sem.handle).collect::<Vec<vk::Semaphore>>();

        let info = vk::SubmitInfo {
            wait_semaphore_count: wait_sem_handles.len() as u32,
            p_wait_semaphores: wait_sem_handles.as_ptr(),

            signal_semaphore_count: signal_sem_handles.len() as u32,
            p_signal_semaphores: signal_sem_handles.as_ptr(),
            p_wait_dst_stage_mask: wait_dst_stage_mask.as_ptr(),

            command_buffer_count: buffer_handles.len() as u32,
            p_command_buffers: buffer_handles.as_ptr(),

            ..Default::default()
        };

        self.dev.handle.queue_submit(
            self.handle,
            &[info],
            if let Some(fence) = fence { fence.handle } else { vk::Fence::null() },
        )
    }

    pub fn present(&self, swapchian: &Swapchain, image_idx: u32, wait_sems: &[&sync::Semaphore]) -> VkResult<bool> {
        let wait_sems = wait_sems.into_iter().map(|sem| sem.handle).collect::<Vec<vk::Semaphore>>();
        let present_info = vk::PresentInfoKHR {
            swapchain_count: 1,
            p_swapchains: &swapchian.handle,
            p_image_indices: &image_idx,

            wait_semaphore_count: wait_sems.len() as u32,
            p_wait_semaphores: wait_sems.as_ptr(),
            ..Default::default()
        };

        unsafe {
            swapchian.dev.swapchain_khr.queue_present(self.handle, &present_info)
        }
    }
}

pub struct CommandPool {
    handle: vk::CommandPool,
    dev: Arc<Device>,
}

impl CommandPool {
    /// Creates a command pool of the given queue type. The caller is responsible for obeying the flags
    /// parameter.
    ///
    /// # PANICS
    /// Panics if there is no family of the givien type.
    pub fn new(dev: Arc<Device>, ty: QueueType , flags: vk::CommandPoolCreateFlags) -> VkResult<CommandPool>
    {
        let family_idx = dev.queue_families.get_index_for_type(ty).unwrap().0;
        let pool_info = vk::CommandPoolCreateInfo {
            queue_family_index: family_idx,
            flags,
            ..Default::default()
        };

        let handle = unsafe { dev.handle.create_command_pool(&pool_info, None)? };

        Ok(CommandPool { handle, dev })
    }

    /// Reset the command pool
    ///
    /// # Safety
    /// May destory in-use buffers
    #[inline]
    pub unsafe fn reset_pool(&self, flags: vk::CommandPoolResetFlags) -> VkResult<()> {
        self.dev.handle.reset_command_pool(self.handle, flags)
    }

    /// Create command buffers.
    pub fn create_command_buffers(&self, count: u32, is_primary: bool) -> VkResult<Vec<CommandBuffer>> {
        let info = vk::CommandBufferAllocateInfo {
            command_pool: self.handle,
            command_buffer_count: count,
            level: if is_primary { vk::CommandBufferLevel::PRIMARY } else { vk::CommandBufferLevel::SECONDARY },
            ..Default::default()
        };

        let buffers = unsafe { self.dev.handle.allocate_command_buffers(&info)? };
        Ok(buffers.into_iter().map(|b| CommandBuffer { is_primary, handle: b, dev: Arc::clone(&self.dev) }).collect())
    }
}

impl Drop for CommandPool {
    fn drop(&mut self) {
        unsafe {
            self.dev.handle.destroy_command_pool(self.handle, None);
        }
    }
}

pub struct CommandBuffer {
    is_primary: bool,
    handle: vk::CommandBuffer,
    dev: Arc<Device>,
}

impl CommandBuffer {
    pub fn dev(&self) -> &Device {
        &self.dev
    }

    /// Begin recording a primary command buffer
    ///
    /// # Safety
    /// Application is fully responsible for ensuring the command buffer is in a state that it is safe to begin from
    pub unsafe fn begin(&self, one_time_use: bool) -> VkResult<()> {
        if !self.is_primary {
            panic!("Only supports primary command buffers");
        }

        let begin_info = vk::CommandBufferBeginInfo {
            flags: if one_time_use {
                vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT
            } else {
                vk::CommandBufferUsageFlags::SIMULTANEOUS_USE
            },
            ..Default::default()
        };

        self.dev.handle.begin_command_buffer(self.handle, &begin_info)
    }

    /// End a primary command buffer
    ///
    /// # Safety
    /// Application is fully responsible for ensuring the command buffer is in a state that it is safe to end from
    pub unsafe fn end(&self) -> VkResult<()> {
        self.dev.handle.end_command_buffer(self.handle)
    }

    /// Use [`ash::Device::cmd_set_viewport`] to set the viewport of index 0 to the parameter.
    ///
    /// # Safety
    /// CommandBuffer must be in the recording state
    pub unsafe fn cmd_set_viewport(&self, viewport: vk::Viewport) {
        // multiple viewports is an extention; if it seems useful I will add an additional method
        self.dev.handle.cmd_set_viewport(self.handle, 0, &[viewport]);
    }

    /// Use [`ash::Device::cmd_set_scissor`] to set the scissor of index 0 to the parameter.
    ///
    /// # Safety
    /// CommandBuffer must be in the recording state
    pub unsafe fn cmd_set_scissor(&self, scissor: vk::Rect2D) {
        self.dev.handle.cmd_set_scissor(self.handle, 0, &[scissor]);
    }

    /// Copy from `src` to `dst`, wraps [`ash::Device::cmd_copy_buffer`]
    ///
    /// # Safety
    /// CommandBuffer must be in the recording state
    pub unsafe fn cmd_copy_buffer(
        &self,
        src: &Buffer,
        dst: &Buffer,
        src_offset: vk::DeviceSize,
        dst_offset: vk::DeviceSize,
        size: vk::DeviceSize,
    ) {
        let info = vk::BufferCopy {
            src_offset,
            dst_offset,
            size,
        };

        self.dev.handle.cmd_copy_buffer(self.handle, src.handle, dst.handle, &[info]);
    }

    /// Bind the buffers as vertex buffers
    ///
    /// # Safety
    /// CommandBuffer must be in the recording state
    pub unsafe fn cmd_bind_vertex_buffers(
        &self,
        first_binding: u32,
        buffers: &[&Buffer],
        offsets: &[vk::DeviceSize],
    ) {
        let buffers = buffers.into_iter().map(|b| {
            b.handle
        }).collect::<Vec<_>>();
        self.dev.handle.cmd_bind_vertex_buffers(self.handle, first_binding, &buffers, offsets)
    }

    pub unsafe fn cmd_pipeline_barrier2(&self, info: &vk::DependencyInfoKHR) {
        self.dev.sync2_khr.cmd_pipeline_barrier2(self.handle, &info);
    }

    pub unsafe fn cmd_copy_buffer_to_image(
        &self,
        src: &Buffer,
        dst: &Image,
        layout: vk::ImageLayout,
        regions: &[vk::BufferImageCopy],
    ) {
        self.dev.handle.cmd_copy_buffer_to_image(self.handle, src.handle, dst.handle, layout, regions);
    }

    pub unsafe fn cmd_draw(&self, vertex_count: u32, instance_count: u32, first_vertex: u32, first_instance: u32) {
        self.dev.handle.cmd_draw(self.handle, vertex_count, instance_count, first_vertex, first_instance);
    }

    pub unsafe fn cmd_blit_image(
        &self,
        src: &Image, src_layout: vk::ImageLayout,
        dst: &Image, dst_layout: vk::ImageLayout,
        regions: &[vk::ImageBlit], filter: vk::Filter,
    ) {
        self.dev.handle.cmd_blit_image(self.handle, src.handle, src_layout, dst.handle, dst_layout, regions, filter)
    }

    pub unsafe fn cmd_bind_descriptor_sets(
        &self, bind_point: vk::PipelineBindPoint, pipe_layout: &pipeline::PipelineLayout,
        first_set: u32, sets: &[vk::DescriptorSet], offs: &[u32]
    ) {
        self.dev.handle.cmd_bind_descriptor_sets(self.handle, bind_point, pipe_layout.handle, first_set, sets, offs)
    }

    pub unsafe fn cmd_push_constants(
        &self,
        layout: &pipeline::PipelineLayout,
        stage_flags: vk::ShaderStageFlags,
        offset: u32,
        constants: &[u8],
    ) {
        self.dev.handle.cmd_push_constants(self.handle, layout.handle, stage_flags, offset, constants)
    }

    #[inline]
    pub fn handle(&self) -> vk::CommandBuffer {
        self.handle
    }
}

// DO NOT IMPLENT OUTSIDE OF THIS CRATE THIS IS UNSTABLE
// it is recommended to use the macro to implment this
#[doc(hidden)]
pub trait NamableImpl {
    fn get_needed_info_for_nameing(&self) -> (u64, &Device, vk::ObjectType);
}

macro_rules! impl_nameable_for_type {
    ($type:ty, $ty:expr) => {
        impl crate::NamableImpl for $type {
            fn get_needed_info_for_nameing(&self) -> (u64, &crate::Device, vk::ObjectType) {
                use ash::vk::Handle;
                return (self.handle.as_raw(), &self.dev, $ty)
            }
        }

        impl crate::Nameable for $type {}
    };
    ($type:ty, $ty:expr, $dev_under:tt) => {
        impl crate::NamableImpl for $type {
            fn get_needed_info_for_nameing(&self) -> (u64, &crate::Device, vk::ObjectType) {
                use ash::vk::Handle;
                return (self.handle.as_raw(), &self.$dev_under.dev, $ty)
            }
        }

        impl crate::Nameable for $type {}
    };
}
pub (crate) use impl_nameable_for_type;

impl_nameable_for_type!(Swapchain, vk::ObjectType::SWAPCHAIN_KHR);
impl_nameable_for_type!(Queue, vk::ObjectType::QUEUE);
impl_nameable_for_type!(CommandPool, vk::ObjectType::COMMAND_POOL);
impl_nameable_for_type!(CommandBuffer, vk::ObjectType::COMMAND_BUFFER);

/// Gives a [`Nameable`] a name via [`std::format`]
///
/// It only does the format if the `vkdebug` feature is enabled
#[macro_export]
macro_rules! format_vulkan_name {
    ($object:expr, $($arg:tt)*) => {
        if cfg!(feature = "vkdebug") {
            let mut string = format!($($arg)*);
            string.push('\0');
            unsafe {
                $object.give_name_unsafe(string.as_ptr() as *const ::std::os::raw::c_char)
            }
        } else {
            Ok(())
        }
    };
}

/// Types with this trait can be given a name for debug usage
///
/// Sometimes the vulkan implementation will re-create a type,
/// when this happens the name is discarded.
pub trait Nameable: NamableImpl {
    /// Gives the handle a name that will show up in debug tools like the validation layers and renderdoc
    #[cfg(feature = "vkdebug")]
    fn give_name(&self, name: &CStr) -> VkResult<()> {
        unsafe { self.give_name_unsafe(name.as_ptr()) }
    }

    /// Gives the handle a name that will show up in debug tools like the validation layers and renderdoc
    #[cfg(not(feature = "vkdebug"))]
    #[inline]
    fn give_name(&self, _name: &CStr) -> VkResult<()> { Ok(()) }

    /// Gives the handle a name that will show up in debug tools like the validation layers and renderdoc
    ///
    /// # Safety
    /// `name` must be a valid c string
    #[cfg(feature = "vkdebug")]
    unsafe fn give_name_unsafe(&self, name: *const c_char) -> VkResult<()> {
        let (handle, dev, ty) = self.get_needed_info_for_nameing();
        let info = vk::DebugUtilsObjectNameInfoEXT {
            object_handle: handle,
            object_type: ty,
            p_object_name: name,
            ..Default::default()
        };
        dev.instance.debug_utils_ext.as_ref().unwrap().debug_utils_set_object_name(dev.handle.handle(), &info)
    }

    /// Gives the handle a name that will show up in debug tools like the validation layers and renderdoc
    ///
    /// # Safety
    /// `name` must be a valid c string
    #[cfg(not(feature = "vkdebug"))]
    #[inline]
    unsafe fn give_name_unsafe(&self, _name: *const c_char) -> VkResult<()> { Ok(()) }
}

unsafe extern "system" fn debug_messenger_callback(sevarity: vk::DebugUtilsMessageSeverityFlagsEXT
        , message_ty: vk::DebugUtilsMessageTypeFlagsEXT
        , callback_data: *const vk::DebugUtilsMessengerCallbackDataEXT
        , _user_data: *mut c_void) -> vk::Bool32 {
    #[inline]
    fn sevarity_to_log_level(s: vk::DebugUtilsMessageSeverityFlagsEXT) -> log::Level{
        match s {
            vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE => log::Level::Debug,
            vk::DebugUtilsMessageSeverityFlagsEXT::ERROR => log::Level::Error,
            vk::DebugUtilsMessageSeverityFlagsEXT::WARNING => log::Level::Warn,
            vk::DebugUtilsMessageSeverityFlagsEXT::INFO => log::Level::Info,
            _ => log::Level::Error,
        }
    }

    // credit to VkBootstrap for the magic numbers
    #[inline]
    fn type_to_string(s: vk::DebugUtilsMessageTypeFlagsEXT) -> &'static str {
        match s.as_raw() {
            7 => "General | Validation | Performance",
            6 => "Validation | Performance",
            5 => "General | Performance",
            4 => "Performance",
            3 => "General | Validation",
            2 => "Validation",
            1 => "General",
            _ => "UNKNOWN MESSAGE TYPE",
        }
    }

    // SAFETY: the vulkan spec states that both of the following pointers are null terminated utf8 strings
    let text = CStr::from_ptr((*callback_data).p_message).to_string_lossy();
    let name = CStr::from_ptr((*callback_data).p_message_id_name).to_string_lossy();
    let sevarity = sevarity_to_log_level(sevarity);

    log::log!(sevarity, "VALIDATION ERROR [{}, {}] {name}:\n{text}"
                , sevarity.as_str(), type_to_string(message_ty));

    return vk::FALSE;
}
