#![allow(clippy::vec_init_then_push)]

#[allow(unused)]
mod shaders {
    include!(concat!(env!("OUT_DIR"), "/shaders.rs"));
}

#[allow(unused)]
mod resources {
    include!(concat!(env!("OUT_DIR"), "/resources.rs"));
}

mod prelude {
    #![allow(unused_imports)]

    pub use anyhow::{anyhow, bail, Context};
    pub use log::{trace, debug, info, warn, error, log_enabled};
    pub use glam::f32::*;
    pub use glam::{const_vec2, const_vec3, const_vec4};
    pub use vktools::{
        format_vulkan_name,
        Nameable,
        sync::WaitForFences,
    };
}

mod level;
mod material;
mod particles;
mod post_processing;
mod text;
mod util;

use crate::prelude::*;

use std::{
    io,
    process,
    sync::Arc,
    time::Instant,
};

use arrayvec::ArrayVec;

use util::transform_matrix;
use vktools::{
    ash::vk,
    mem::{Allocator, Sampler, Image, MemoryUsage},
    rcstr,
    sync::{Fence, Semaphore},
    pipeline::{ShaderModule, DescriptorAllocator, DescriptorLayoutCache},
    CommandPool, CommandBuffer,
};

use winit::{
    dpi::PhysicalSize,
    event_loop::EventLoop,
    platform::run_return::EventLoopExtRunReturn,
    window::{Window, WindowBuilder},
};

#[cfg(feature = "x11")]
use winit::platform::unix::WindowBuilderExtUnix;

use crate::{
    level::{Level, SpriteRenderer, SquareGameObject, Ball, PowerUpType, PowerUp},
    material::{Material, GlobalUniformBuffer, GlobalUniformBufferContents, Texture},
    particles::ParticleEmmiter,
    post_processing::{PostProcessing, PostProcessingUniformBuffer, PostProcessingUniformContents,
                        create_material_for_post_proccessing},
};

const WINDOW_SIZE: vk::Extent2D = vk::Extent2D {
    width: 800,
    height: 600,
};

fn main() {
    match try_main() {
        Ok(_) => (),
        Err(e) => {
            eprintln!("error: {}", e);
            process::exit(1);
        },
    }
}

fn try_main() -> anyhow::Result<()> {
    #[cfg(debug_assertions)]
    env_logger::builder()
        .filter_level(log::LevelFilter::Debug)
        .init();

    #[cfg(not(debug_assertions))]
    env_logger::init();

    let mut el = EventLoop::new();
    let win = {
        let builder = WindowBuilder::new()
            .with_title("breakout")
            .with_resizable(false)
            .with_inner_size(PhysicalSize::new(WINDOW_SIZE.width, WINDOW_SIZE.height));

        #[cfg(feature = "x11")]
        let builder = builder.with_class("Breakout".to_owned(), "breakout".to_owned());

        builder.build(&el)
    }?;

    let mut game = init_game(Arc::new(win))?;

    let mut result: Option<anyhow::Result<()>> = None;
    // run_return is needed to get destructors called
    el.run_return(|event, _, control_flow| {
        use winit::event_loop::ControlFlow;

        let res = game.process_events(event, control_flow);
        if res.is_err() {
            error!("Fatal error occurred during update");
            *control_flow = ControlFlow::Exit;
            result = Some(res);
        }
    });

    game.dev.wait_idle()?;

    if let Some(result) = result {
        return result;
    }

    Ok(())
}

fn init_game(win: Arc<Window>) -> anyhow::Result<Game> {
    let inst_win = Arc::clone(&win);
    let instance = Arc::new(vktools::Instance::new(
            rcstr!("breakout"),
            vk::make_api_version(0, 1, 0, 0),
            inst_win)?
    );
    let req_format_features = [
        (vk::Format::R8G8B8A8_SRGB, vk::FormatProperties {
            optimal_tiling_features: vk::FormatFeatureFlags::SAMPLED_IMAGE_FILTER_LINEAR,
            ..Default::default()
        }),
    ];
    let physical_dev = vktools::PhysicalDevice::pick(Arc::clone(&instance), &req_format_features)?;
    let gq_cnt = physical_dev.grphics_queue_count();
    let tq_cnt = physical_dev.transfer_only_queue_count();

    if gq_cnt.is_none() || gq_cnt.unwrap() < 1 {
        bail!("Requires graphics card that supports graphics queues");
    }

    if tq_cnt.is_none() || tq_cnt.unwrap() < 1 {
        bail!("Currently requires a graphics card that has a transfer only queue");
    }

    let dev = Arc::new(unsafe {
        physical_dev.build_device()
            .request_queue(vktools::QueueType::Graphics, 2, &[1.0, 1.0])
            //.request_queue(vktools::QueueType::TransferOnly, 1, &[1.0])
            .build()?
    });

    // SAFETY: preconditions checked above
    // SAFETY: safe if the device was built correctly
    let graphics_queue = unsafe {
        vktools::Queue::get_from_device(Arc::clone(&dev), vktools::QueueType::Graphics, 0)
    };

    // SAFETY: safe if the device was built correctly
    let transfer_queue = unsafe {
        vktools::Queue::get_from_device(Arc::clone(&dev), vktools::QueueType::Graphics, 1)
    };

    let swapchain = vktools::Swapchain::new(Arc::clone(&dev), WINDOW_SIZE)?;

    let alloc = Arc::new(Allocator::new(Arc::clone(&dev))?);

    let post_proc_uniform_buf = PostProcessingUniformBuffer::new(Arc::clone(&alloc), FRAME_OVERLAP as u64)?;
    let mut post_proc_uniform_contents = PostProcessingUniformContents::default();

    let offset = 1.0 / 300.0;
    let offsets = [
        vec2(-offset,  offset),
        vec2( 0.0,     offset),
        vec2( offset,  offset),
        vec2(-offset,  0.0   ),
        vec2( 0.0,     0.0   ),
        vec2( offset,  0.0   ),
        vec2(-offset, -offset),
        vec2( 0.0,    -offset),
        vec2( offset, -offset),
    ];
    post_proc_uniform_contents.offsets = offsets;

    let edge_kernel = [
        1,  1, 1,
        1, -8, 1,
        1,  1, 1,
    ];
    post_proc_uniform_contents.edge_kernel = edge_kernel;

    let blur_kernel = [
        1.0 / 16.0, 2.0 / 16.0, 1.0 / 16.0,
        2.0 / 16.0, 4.0 / 16.0, 2.0 / 16.0,
        1.0 / 16.0, 2.0 / 16.0, 1.0 / 16.0,
    ];

    post_proc_uniform_contents.blur_kernel = blur_kernel;

    let mut descriptor_alloc = DescriptorAllocator::new(Arc::clone(&dev));
    let mut descriptor_cache = DescriptorLayoutCache::new(Arc::clone(&dev));
    let uploader = vktools::util::Uploader::new(Arc::clone(&dev), transfer_queue)?;

    let default_sampler = Sampler::new(Arc::clone(&dev)
                              , vk::Filter::LINEAR, vk::SamplerAddressMode::REPEAT,
                              vk::FALSE, None, None)?;
    let default_sampler = Arc::new(default_sampler);

    let post_proc_mat = create_material_for_post_proccessing(Arc::clone(&dev),
            &mut descriptor_cache, swapchain.image_format())?;
    let post_proc_mat = Arc::new(post_proc_mat);

    let mut frame_data = ArrayVec::<_, FRAME_OVERLAP>::new();
    for _ in 0..FRAME_OVERLAP {
        let cmd_pool = CommandPool::new(
            Arc::clone(&dev),
            vktools::QueueType::Graphics,
            vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER
        )?;

        let buffers = cmd_pool.create_command_buffers(1, true)?;

        let present_sem = Semaphore::new(Arc::clone(&dev))?;
        present_sem.give_name(rcstr!("Present Semaphore"))?;
        let render_sem = Semaphore::new(Arc::clone(&dev))?;
        present_sem.give_name(rcstr!("Render Semaphore"))?;
        let render_fence = Fence::new(Arc::clone(&dev), vk::FenceCreateFlags::SIGNALED)?;
        present_sem.give_name(rcstr!("Render Fence"))?;

        let render_image_extent = swapchain.extent();
        let render_image_extent = vk::Extent3D {
            width: render_image_extent.width,
            height: render_image_extent.height,
            depth: 1,
        };

        let render_image = unsafe { Image::new(Arc::clone(&alloc), MemoryUsage::GpuOnly,
                vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::SAMPLED,
                swapchain.image_format(),
                render_image_extent, vk::ImageType::TYPE_2D, 1, 1, vk::ImageAspectFlags::COLOR,
                None) }?;
        render_image.give_name(rcstr!("Render Image"))?;

        let view_mapping = vk::ComponentMapping {
            r: vk::ComponentSwizzle::IDENTITY,
            g: vk::ComponentSwizzle::IDENTITY,
            b: vk::ComponentSwizzle::IDENTITY,
            a: vk::ComponentSwizzle::IDENTITY,
        };
        let render_image_view = render_image.create_view(vk::ImageViewType::TYPE_2D, view_mapping)?;

        let render_image = Texture { image: render_image, view: render_image_view };

        let post_proc = PostProcessing::new(&dev, Arc::clone(&post_proc_mat),
                                            Arc::clone(&default_sampler),
                                            &mut descriptor_alloc,
                                            &post_proc_uniform_buf)?;

        frame_data.push(FrameData {
            present_sem, render_sem, render_fence, cmd_pool, buffers, render_image, post_proc,
        });
    }

    let shaders = vec![
        ShaderModule::from_reader(
            Arc::clone(&dev),
            vk::ShaderStageFlags::VERTEX,
            &mut io::Cursor::new(shaders::MESH_VERT)
        )?,
        ShaderModule::from_reader(
            Arc::clone(&dev),
            vk::ShaderStageFlags::FRAGMENT,
            &mut io::Cursor::new(shaders::TEXTURED_FRAG)
        )?,
    ];

    let textured_mesh = Material::new(
        &mut descriptor_cache, Arc::clone(&dev), shaders, swapchain.image_format(), false)?;
    let textured_mesh = Arc::new(textured_mesh);

    let proj = Mat4::orthographic_rh(0.0, 800.0, 0.0, 600.0, -1.0, 1.0);
    let global_uniform = GlobalUniformBufferContents {
        proj,
    };
    let global_uniform_buf = GlobalUniformBuffer::new(Arc::clone(&alloc), FRAME_OVERLAP as u64)?;

    let mut sprite_renderer = SpriteRenderer::new(
        Arc::clone(&alloc),
        &uploader,
        textured_mesh,
        Arc::clone(&default_sampler),
    )?;

    sprite_renderer.add_texture("sample".to_owned(), &alloc, &uploader, &mut descriptor_alloc
                                , io::Cursor::new(resources::COPYRIGHT_FREE_IMAGE_PNG)
                                , &global_uniform_buf)?;
    sprite_renderer.add_texture("block".to_owned(), &alloc, &uploader, &mut descriptor_alloc
                                , io::Cursor::new(resources::BRICK_PNG)
                                , &global_uniform_buf)?;
    sprite_renderer.add_texture("block-solid".to_owned(), &alloc, &uploader, &mut descriptor_alloc
                                , io::Cursor::new(resources::SOLID_BRICK_PNG)
                                , &global_uniform_buf)?;
    sprite_renderer.add_texture("background".to_owned(), &alloc, &uploader, &mut descriptor_alloc
                                , io::Cursor::new(resources::BACKGROUND_PNG)
                                , &global_uniform_buf)?;
    sprite_renderer.add_texture("paddle".to_owned(), &alloc, &uploader, &mut descriptor_alloc
                                , io::Cursor::new(resources::PADDLE_PNG)
                                , &global_uniform_buf)?;
    sprite_renderer.add_texture("ball".to_owned(), &alloc, &uploader, &mut descriptor_alloc
                                , io::Cursor::new(resources::BALL_PNG)
                                , &global_uniform_buf)?;
    sprite_renderer.add_texture("powerup-speed".to_owned(), &alloc, &uploader, &mut descriptor_alloc
                                , io::Cursor::new(resources::POWERUP_SPEED_PNG)
                                , &global_uniform_buf)?;
    sprite_renderer.add_texture("powerup-sticky".to_owned(), &alloc, &uploader, &mut descriptor_alloc
                                , io::Cursor::new(resources::POWERUP_STICKY_PNG)
                                , &global_uniform_buf)?;
    sprite_renderer.add_texture("powerup-pass-through".to_owned(), &alloc, &uploader, &mut descriptor_alloc
                                , io::Cursor::new(resources::POWERUP_PASS_THROUGH_PNG)
                                , &global_uniform_buf)?;
    sprite_renderer.add_texture("powerup-big-paddle".to_owned(), &alloc, &uploader, &mut descriptor_alloc
                                , io::Cursor::new(resources::POWERUP_BIG_PADDLE_PNG)
                                , &global_uniform_buf)?;
    sprite_renderer.add_texture("powerup-confuse".to_owned(), &alloc, &uploader, &mut descriptor_alloc
                                , io::Cursor::new(resources::POWERUP_CONFUSE_PNG)
                                , &global_uniform_buf)?;
    sprite_renderer.add_texture("powerup-chaos".to_owned(), &alloc, &uploader, &mut descriptor_alloc
                                , io::Cursor::new(resources::POWERUP_CHAOS_PNG)
                                , &global_uniform_buf)?;

    let level_extent = vk::Extent2D {
        width: WINDOW_SIZE.width,
        height: WINDOW_SIZE.height / 2,
    };
    let levels = vec![
        Level::load(io::Cursor::new(resources::ONE_LVL),   level_extent)?,
        Level::load(io::Cursor::new(resources::TWO_LVL),   level_extent)?,
        Level::load(io::Cursor::new(resources::THREE_LVL), level_extent)?,
        Level::load(io::Cursor::new(resources::FOUR_LVL),  level_extent)?,
        Level::load(io::Cursor::new(resources::SIMPLE_LVL),  level_extent)?,
    ];

    let player_pos = vec2(WINDOW_SIZE.width as f32 / 2.0 - PLAYER_SIZE.x / 2.0,
                          WINDOW_SIZE.height as f32 - PLAYER_SIZE.y);
    let player = SquareGameObject::new(player_pos, PLAYER_SIZE, true, Vec3::from([3.0; 3]), "paddle");

    let ball_pos = player_pos + vec2(PLAYER_SIZE.x / 2.0 - BALL_RADIUS, -BALL_RADIUS * 2.0);
    let ball = Ball::new(ball_pos, BALL_RADIUS, BALL_VELOCITY);

    log::info!("loading texture ball-particle");
    let particle_tex = material::Texture::from_png(Arc::clone(&alloc), &uploader
                                                   , io::Cursor::new(resources::BALL_PARTICLE_PNG))?;
    let ball_emmiter = ParticleEmmiter::new(Arc::clone(&dev),
            Arc::clone(&alloc), &mut descriptor_alloc, &mut descriptor_cache,
            swapchain.image_format(), &uploader, Arc::new(particle_tex),
            Arc::clone(&default_sampler), &global_uniform_buf)?;

    let text_renderer = text::TextRenderer::new(Arc::clone(&dev), Arc::clone(&alloc), &mut descriptor_cache, &mut descriptor_alloc, swapchain.image_format(), &uploader, &global_uniform_buf)?;

    Ok(Game {
        dev,
        win,
        swapchain,
        graphics_queue,

        start_time: Instant::now(),
        last_draw: Instant::now(),

        descriptor_alloc,
        descriptor_cache,

        default_sampler,

        global_uniform,
        global_uniform_buf,

        post_proc_uniform_buf,
        post_proc_uniform_contents,
        shake_time: 0.0,

        keys: [false; Keys::LENGTH as usize],
        processed_keys: [false; Keys::LENGTH as usize],

        rng: rand_pcg::Pcg32::new(0xcafef00dd15ea5e5, 0xa02bdbf7bb3c0a7),

        sprite_renderer,
        text_renderer,

        levels,
        curr_level: 0,
        state: GameState::Menu,

        player,
        ball,
        ball_emmiter,
        power_ups: vec![],

        frame_counter: 0,
        // SAFETY: filled above
        frames: unsafe { frame_data.into_inner_unchecked() }
    })
}

const FRAME_OVERLAP: usize = 2;

const PLAYER_SIZE: Vec2 = const_vec2!([100.0, 20.0]);
const PADDLE_VELOCITY: f32 = 500.0;

const BALL_RADIUS: f32 = 12.5;
const BALL_VELOCITY: Vec2 = const_vec2!([100.0, -350.0]);

struct FrameData {
    present_sem: Semaphore,
    render_sem: Semaphore,
    render_fence: Fence,

    render_image: Texture,
    post_proc: PostProcessing,

    cmd_pool: CommandPool,
    buffers: Vec<CommandBuffer>,
}

#[derive(Clone, Copy, PartialEq, Eq)]
enum GameState {
    Active,
    Menu,
    Win,
}

#[repr(usize)]
enum Keys {
    W = 0,
    A,
    S,
    D,
    Return,
    Space,
    LENGTH,
}

#[allow(unused)]
struct Game {
    dev: Arc<vktools::Device>,
    win: Arc<Window>,
    swapchain: vktools::Swapchain,
    graphics_queue: vktools::Queue,

    start_time: Instant,
    last_draw: Instant,

    descriptor_alloc: DescriptorAllocator,
    descriptor_cache: DescriptorLayoutCache,

    default_sampler: Arc<Sampler>,

    global_uniform: GlobalUniformBufferContents,
    global_uniform_buf: GlobalUniformBuffer,

    post_proc_uniform_buf: PostProcessingUniformBuffer,
    post_proc_uniform_contents: PostProcessingUniformContents,
    shake_time: f32,

    keys: [bool; Keys::LENGTH as usize],
    processed_keys: [bool; Keys::LENGTH as usize],

    rng: rand_pcg::Pcg32,

    sprite_renderer: SpriteRenderer,
    text_renderer: text::TextRenderer,

    state: GameState,
    levels: Vec<Level>,
    curr_level: usize,

    player: SquareGameObject,
    ball: Ball,
    ball_emmiter: ParticleEmmiter,
    power_ups: Vec<PowerUp>,

    frame_counter: usize,
    frames: [FrameData; FRAME_OVERLAP],
}

impl Game {
    fn process_events(&mut self, event: winit::event::Event<()>
              , control_flow: &mut winit::event_loop::ControlFlow) -> anyhow::Result<()>
    {
        use winit::{
            event::{Event, WindowEvent, VirtualKeyCode},
            event_loop::ControlFlow,
        };

        let PhysicalSize { width, height } = self.win.inner_size();
        if width == 0 || height == 0 {
            *control_flow = ControlFlow::Wait;
        } else {
            *control_flow = ControlFlow::Poll;
        }

        match event {
            Event::WindowEvent { event, .. } => {
                match event {
                    WindowEvent::KeyboardInput { input, .. } => {
                        use winit::event::ElementState::*;

                        if input.virtual_keycode.is_some() {
                            let vkeycode = input.virtual_keycode.unwrap();

                            if vkeycode == VirtualKeyCode::Escape {
                                *control_flow = ControlFlow::Exit;
                                info!("Got escape, shutting down ...");
                            }

                            macro_rules! key {
                                ($key:tt) => {
                                    key!($key, $key)
                                };
                                ($key:tt, $as:tt) => {
                                    if vkeycode == VirtualKeyCode::$key {
                                        match input.state {
                                            Pressed => {
                                                self.keys[Keys::$as as usize] = true;
                                                self.processed_keys[Keys::$as as usize] = false;
                                            },
                                            Released => self.keys[Keys::$as as usize] = false,
                                        }
                                    }
                                }
                            }

                            key!(W);
                            key!(A);
                            key!(S);
                            key!(D);
                            key!(Up, W);
                            key!(Down, S);
                            key!(Left, A);
                            key!(Right, D);
                            key!(Return);
                            key!(Space);
                        }
                    },
                    WindowEvent::Resized(val) => {
                        self.recreate_swapchain(val)?;
                    },

                    WindowEvent::CloseRequested | WindowEvent::Destroyed => {
                        *control_flow = ControlFlow::Exit;
                        info!("Got window close event, shutting down ...");
                    }

                    _ => (),
                }
            },
            Event::MainEventsCleared => {
                if width == 0 || height == 0 {
                    return Ok(());
                }
                self.update();
                self.draw()?;
            },

            _ => (),
        }

        Ok(())
    }

    #[inline]
    fn frame_idx(&self) -> usize {
        self.frame_counter % FRAME_OVERLAP
    }

    fn recreate_swapchain(&mut self, _new_size: winit::dpi::PhysicalSize<u32>) -> Result<(), vk::Result> {
        self.win.set_resizable(false);
        let new_size = winit::dpi::PhysicalSize::<u32> {
            width: 800,
            height: 600,
        };
        self.win.set_inner_size(new_size);

        let extent = vk::Extent2D {
            width: new_size.width,
            height: new_size.height,
        };

        self.swapchain.recreate(extent)?;

        Ok(())
    }

    fn reset(&mut self) {
        let player_pos = vec2(WINDOW_SIZE.width as f32 / 2.0 - PLAYER_SIZE.x / 2.0,
                              WINDOW_SIZE.height as f32 - PLAYER_SIZE.y);
        self.player = SquareGameObject::new(player_pos, PLAYER_SIZE, true, Vec3::from([3.0; 3]), "paddle");

        let ball_pos = player_pos + vec2(PLAYER_SIZE.x / 2.0 - BALL_RADIUS, -BALL_RADIUS * 2.0);
        self.ball = Ball::new(ball_pos, BALL_RADIUS, BALL_VELOCITY);

        for brick in self.levels[self.curr_level].bricks.iter_mut() {
            brick.destroyed = false;
        }

        self.power_ups.clear();

        self.ball.sticky = false;
        self.player.color = const_vec3!([3.0; 3]);
        self.ball.pass_through = false;
        self.ball.color = const_vec3!([1.0; 3]);
        self.post_proc_uniform_contents.confuse = vk::FALSE;
        self.post_proc_uniform_contents.chaos = vk::FALSE;
        self.post_proc_uniform_contents.shake = vk::FALSE;

        self.state = GameState::Menu;
    }

    fn update(&mut self) {
        let this_draw = Instant::now();
        let dt = (this_draw - self.last_draw).as_secs_f32();
        self.last_draw = this_draw;

        macro_rules! key {
            ($key:tt) => {
                self.keys[Keys::$key as usize]
            }
        }

        macro_rules! processed_key {
            ($key:tt) => {
                self.processed_keys[Keys::$key as usize]
            }
        }

        macro_rules! set_proccesed {
            ($key:tt) => {
                self.processed_keys[Keys::$key as usize] = true;
            }
        }

        if self.state == GameState::Active {
            let paddle_velocity = PADDLE_VELOCITY * dt;

            const MAX_PADDLE_OFF_FRAME: f32 = 0.0;
            if key!(A) {
                self.player.pos.x -= paddle_velocity;
                if self.player.pos.x < -MAX_PADDLE_OFF_FRAME {
                    self.player.pos.x = -MAX_PADDLE_OFF_FRAME;
                }
            }
            if key!(D) {
                self.player.pos.x += paddle_velocity;
                if self.player.pos.x > WINDOW_SIZE.width as f32 - PLAYER_SIZE.x + MAX_PADDLE_OFF_FRAME {
                    self.player.pos.x = WINDOW_SIZE.width as f32 - PLAYER_SIZE.x + MAX_PADDLE_OFF_FRAME;
                }
            }

            if key!(Space) {
                self.ball.stopped = false;
            }

            self.ball.update_pos(dt, WINDOW_SIZE);
            if self.ball.stopped {
                let ball_pos = self.player.pos + vec2(PLAYER_SIZE.x / 2.0 - BALL_RADIUS, -BALL_RADIUS * 2.0);
                self.ball.pos = ball_pos;
            }

            self.do_collisions();

            if self.ball.pos.y >= WINDOW_SIZE.height as f32 {
                self.reset();
            }

            // this has to be a separate loop because of the stupid barrow checker
            for pu in self.power_ups.iter_mut() {
                pu.pos += pu.velocity * dt;
                if pu.activated {
                    pu.duration -= dt;
                    if pu.duration <= 0.0 {
                        pu.activated = false;
                    }
                }
            }

            for (i, pu) in self.power_ups.iter().enumerate() {
                fn is_otherpu_active(pu_list: &[PowerUp], ty: PowerUpType) -> bool {
                    for pu in pu_list {
                        if pu.activated && pu.ty == ty {
                            return true;
                        }
                    }

                    return false;
                }


                if !pu.activated && !is_otherpu_active(&self.power_ups[i..], pu.ty) {
                    match pu.ty {
                        PowerUpType::Sticky => {
                            self.ball.sticky = false;
                            self.player.color = const_vec3!([3.0; 3]);
                        },
                        PowerUpType::PassThrough => {
                            self.ball.pass_through = false;
                            self.ball.color = const_vec3!([1.0; 3]);
                        },
                        PowerUpType::Confuse => {
                            self.post_proc_uniform_contents.confuse = vk::FALSE;
                        },
                        PowerUpType::Chaos => {
                            self.post_proc_uniform_contents.chaos = vk::FALSE;
                        },
                        PowerUpType::BigPaddle => (),
                        PowerUpType::Speed => (),
                    }
                }
            }
            if self.levels[self.curr_level].is_completed() {
                self.reset();
                self.state = GameState::Win;
            }
        }

        if self.state == GameState::Menu {
            if key!(Return) && !processed_key!(Return) {
                self.state = GameState::Active;
                set_proccesed!(Return);
            }

            if key!(W) && !processed_key!(W) {
                self.curr_level = (self.curr_level + 1) % self.levels.len();
                set_proccesed!(W);
            }

            if key!(S) && !processed_key!(S) {
                if self.curr_level > 0 {
                    self.curr_level -= 1;
                } else {
                    self.curr_level = self.levels.len() - 1;
                }
                set_proccesed!(S);
            }
        }

        if self.state == GameState::Win {
            if key!(Return) && !processed_key!(Return) {
                self.state = GameState::Menu;
                set_proccesed!(Return);
            }
        }

        let time = self.start_time.elapsed().as_secs_f32();

        self.ball_emmiter.update(dt, &self.ball, &mut self.rng);

        if self.shake_time > 0.0 {
            self.shake_time -= dt;
            self.post_proc_uniform_contents.shake = vk::TRUE;
        } else {
            self.post_proc_uniform_contents.shake = vk::FALSE;
        }

        self.post_proc_uniform_contents.time = time;
    }

    fn do_collisions(&mut self) {
        fn spawn_powerup(pu_list: &mut Vec<PowerUp>, rng: &mut impl rand_core::RngCore, block: &SquareGameObject) {
            fn should_spawn(rng: &mut impl rand_core::RngCore, chance: u32) -> bool {
                return rng.next_u32() % chance == 0;
            }

            const GOOD_CHANCE: u32 = 35;
            const BAD_CHANCE: u32 = 10;

            if should_spawn(rng, GOOD_CHANCE) {
                let ty = PowerUpType::Speed;
                pu_list.push(PowerUp::new(ty, 0.0, block.pos, vec3(0.5, 0.5, 1.0), ty.as_str()));
            }
            if should_spawn(rng, GOOD_CHANCE) {
                let ty = PowerUpType::Sticky;
                pu_list.push(PowerUp::new(ty, 20.0, block.pos, vec3(1.0, 0.5, 1.0), ty.as_str()));
            }
            if should_spawn(rng, GOOD_CHANCE) {
                let ty = PowerUpType::PassThrough;
                pu_list.push(PowerUp::new(ty, 10.0, block.pos, vec3(0.5, 1.0, 0.5), ty.as_str()));
            }
            if should_spawn(rng, GOOD_CHANCE) {
                let ty = PowerUpType::BigPaddle;
                pu_list.push(PowerUp::new(ty, 0.0, block.pos, vec3(1.0, 0.6, 0.4), ty.as_str()));
            }
            if should_spawn(rng, BAD_CHANCE) {
                let ty = PowerUpType::Confuse;
                pu_list.push(PowerUp::new(ty, 15.0, block.pos, vec3(1.0, 0.3, 0.3), ty.as_str()));
            }
            if should_spawn(rng, BAD_CHANCE) {
                let ty = PowerUpType::Chaos;
                pu_list.push(PowerUp::new(ty, 15.0, block.pos, vec3(0.9, 0.25, 0.25), ty.as_str()));
            }
        }

        for b in self.levels[self.curr_level].bricks.iter_mut() {
            if !b.destroyed {
                let collision = level::aabb_ball_collide(*b, self.ball);
                if collision.0 {
                    if !b.is_solid {
                        b.destroyed = true;
                        spawn_powerup(&mut self.power_ups, &mut self.rng, b);
                        if self.ball.pass_through {
                            continue;
                        }
                    } else {
                        self.shake_time += 0.05;
                    }
                    let dir = collision.1;
                    let diff = collision.2;
                    use level::Direction::*;
                    match dir {
                        Left | Right => {
                            self.ball.velocity.x = -self.ball.velocity.x;
                            let penetration = self.ball.radius - diff.x.abs();
                            if dir == Left {
                                self.ball.pos += penetration;
                            } else {
                                self.ball.pos -= penetration;
                            }
                        },
                        Up | Down => {
                            self.ball.velocity.y = -self.ball.velocity.y;
                            let penetration = self.ball.radius - diff.y.abs();
                            if dir == Down {
                                self.ball.pos += penetration;
                            } else {
                                self.ball.pos -= penetration;
                            }
                        }
                    }
                }
            }
        }
        for pu in self.power_ups.iter_mut() {
            if !pu.destroyed {
                if pu.pos.y >= WINDOW_SIZE.height as f32 {
                    pu.destroyed = true;
                }
                if level::aabb_aabb_collide(self.player, *pu) {
                    match pu.ty {
                        PowerUpType::Speed => self.ball.velocity *= 1.2,
                        PowerUpType::Sticky => {
                            self.ball.sticky = true;
                            self.player.color += vec3(0.2, 0.2, 0.2);
                        },
                        PowerUpType::PassThrough => {
                            self.ball.pass_through = true;
                            self.ball.color += vec3(0.2, 0.2, 0.2);
                        },
                        PowerUpType::BigPaddle => self.player.size.x += 50.0,
                        PowerUpType::Confuse => if self.post_proc_uniform_contents.chaos != vk::TRUE {
                            self.post_proc_uniform_contents.confuse = vk::TRUE;
                        },
                        PowerUpType::Chaos => if self.post_proc_uniform_contents.confuse != vk::TRUE {
                            self.post_proc_uniform_contents.chaos = vk::TRUE;
                        },
                    }
                    pu.activated = true;
                    pu.destroyed = true;
                }
            }
        }

        let collision = level::aabb_ball_collide(self.player, self.ball);
        if !self.ball.stopped && collision.0 {
            let board_center = self.player.pos.x + self.player.size.x / 2.0;
            let center_dist = (self.ball.pos.x + self.ball.radius) - board_center;
            let bounce_percent = center_dist / (self.player.size.x / 2.0);

            self.ball.stopped = self.ball.sticky;

            let bounce_strength = 2.0;
            let old_velocity = self.ball.velocity;
            self.ball.velocity.x = BALL_VELOCITY.x * bounce_percent * bounce_strength;
            self.ball.velocity.y = -(self.ball.velocity.y.abs());
            self.ball.velocity = self.ball.velocity.normalize() * old_velocity.length();
        }
    }

    fn draw(&mut self) -> anyhow::Result<()> {
        let frame_idx = self.frame_idx();
        unsafe {
            self.global_uniform_buf.write_contents(
                self.global_uniform,
                frame_idx);
            self.post_proc_uniform_buf.write_contents(
                self.post_proc_uniform_contents, frame_idx);
        }
        self.sprite_renderer.begin_frame(self.frame_idx());

        let data = &mut self.frames[frame_idx];

        data.render_fence.wait_for(1000000000)
            .context("Ether the previous frame took more than 1 second to render or the program is deadlocked")?;

        let next_image_result = self.swapchain
            .aquire_next_image_index(1000000000, Some(&data.present_sem), None);
        let mut image_index: Option<u32> = None;
        let mut suboptimal = false;
        let mut out_of_date = false;
        match next_image_result {
            Ok((idx, subopt)) => {
                suboptimal = subopt;
                image_index = Some(idx);
            }
            Err(e) => {
                match e {
                    vk::Result::ERROR_OUT_OF_DATE_KHR => {
                        out_of_date = true;
                    }
                    _ => {
                        return Err(e).context("Failed to aquire image to render to");
                    }
                }
            }
        }
        if out_of_date {
            info!("Swapchain out of date ... recreating");
            self.recreate_swapchain(self.win.inner_size())?;
            return Ok(());
        }


        data.render_fence.reset().unwrap();
        let image_index = image_index.unwrap();

        format_vulkan_name!(data.buffers[0], "Main Command Buffer")?;

        unsafe {
            data.cmd_pool.reset_pool(vk::CommandPoolResetFlags::empty())?;

            data.buffers[0].begin(true)?;
            {
                let buf = &data.buffers[0];
                let clear_color = make_clear_color([0.0, 0.0, 0.0, 1.0]);
                let render_area = vk::Rect2D {
                    offset: vk::Offset2D { x: 0, y: 0 },
                    extent: self.swapchain.extent(),
                };

                let render_image = &mut data.render_image.image;
                let render_image_view = &data.render_image.view;

                render_image.cmd_change_layout(
                    buf, vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                    vk::AccessFlags2KHR::COLOR_ATTACHMENT_WRITE,
                    vk::PipelineStageFlags2KHR::ALL_GRAPHICS,
                    vk::PipelineStageFlags2KHR::COLOR_ATTACHMENT_OUTPUT);

                // main rendering
                vktools::begin_rendering(
                    &self.dev,
                    buf,
                    render_image_view.handle(),
                    //self.swapchain.image_view_with_index(image_index as usize),
                    clear_color,
                    render_area,
                ); {
                    let viewport = vk::Viewport {
                        x: 0.0,
                        y: 0.0,
                        width: self.swapchain.extent().width as f32,
                        height: self.swapchain.extent().height as f32,
                        min_depth: 0.0,
                        max_depth: 1.0,
                    };
                    buf.cmd_set_viewport(viewport);

                    let scissor = vk::Rect2D {
                        offset: vk::Offset2D { x: 0, y: 0 },
                        extent: self.swapchain.extent(),
                    };
                    buf.cmd_set_scissor(scissor);

                    self.sprite_renderer.cmd_draw_spirite(buf, "background"
                                                          , transform_matrix(vec2(0.0, 0.0), vec2(800.0, 600.0), 0.0)
                                                          , vec3(3.0, 3.0, 3.0));
                    self.levels[self.curr_level].cmd_draw(buf, &mut self.sprite_renderer);
                    if !self.ball.stopped {
                        self.ball_emmiter.cmd_draw(buf, frame_idx);
                    }
                    self.player.cmd_draw(buf, &mut self.sprite_renderer);

                    for pu in self.power_ups.iter() {
                        if !pu.destroyed {
                            pu.cmd_draw(buf, &mut self.sprite_renderer);
                        }
                    }

                    self.ball.cmd_draw(buf, &mut self.sprite_renderer);

                    #[allow(unused_variables)]
                    let vk::Extent2D { width, height } = self.swapchain.extent();

                    if self.state == GameState::Menu {
                        self.text_renderer.cmd_draw_line(buf, "Press RETURN to start",
                                vec2(250.0, height as f32 / 2.0 + 10.0), 0.50,
                                const_vec3!([0.75; 3]), frame_idx);
                        self.text_renderer.cmd_draw_line(buf, "Press W/UP or S/DOWN to select level",
                                vec2(150.0, height as f32 / 2.0 + 35.0), 0.50,
                                const_vec3!([0.75; 3]), frame_idx);
                    }

                    if self.state == GameState::Win {
                        self.text_renderer.cmd_draw_line(buf, "You Win",
                                vec2(320.0, height as f32 / 2.0), 1.0,
                                const_vec3!([0.8; 3]), frame_idx);
                        self.text_renderer.cmd_draw_line(buf, "Press RETURN to replay",
                                vec2(275.0, height as f32 / 2.0 + 35.0), 0.50,
                                const_vec3!([0.75; 3]), frame_idx);
                    }
                }

                vktools::end_rendering(&self.dev, buf);

                render_image.cmd_change_layout(buf,
                        vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                        vk::AccessFlags2KHR::SHADER_READ,
                        vk::PipelineStageFlags2KHR::ALL_GRAPHICS,
                        vk::PipelineStageFlags2KHR::FRAGMENT_SHADER);

                let image_barrier = vk::ImageMemoryBarrier2KHR {
                    src_stage_mask: vk::PipelineStageFlags2KHR::COLOR_ATTACHMENT_OUTPUT_KHR,
                    dst_stage_mask: vk::PipelineStageFlags2KHR::ALL_COMMANDS,
                    src_access_mask: vk::AccessFlags2KHR::COLOR_ATTACHMENT_WRITE,
                    dst_access_mask: vk::AccessFlags2KHR::empty(),
                    old_layout: vk::ImageLayout::UNDEFINED,
                    new_layout: vk::ImageLayout::PRESENT_SRC_KHR,
                    image: self.swapchain.image_with_index(image_index as usize),
                    subresource_range: vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: vk::REMAINING_MIP_LEVELS,
                        base_array_layer: 0,
                        layer_count: vk::REMAINING_ARRAY_LAYERS,
                    },
                    ..Default::default()
                };

                let barrier_info = vk::DependencyInfoKHR {
                    image_memory_barrier_count: 1,
                    p_image_memory_barriers: &image_barrier,
                    ..Default::default()
                };

                self.dev.sync2_khr().cmd_pipeline_barrier2(buf.handle(), &barrier_info);

                // I have no idea if this is the correct thing to do
                data.post_proc.write_image(&self.dev, &data.render_image);

                // post processing
                vktools::begin_rendering(
                    &self.dev,
                    buf,
                    self.swapchain.image_view_with_index(image_index as usize),
                    clear_color,
                    render_area,
                ); {
                    data.post_proc.cmd_do(buf, frame_idx as usize);
                }
                vktools::end_rendering(&self.dev, buf);
            }
            data.buffers[0].end()?;

            self.graphics_queue.submit(
                &[&data.buffers[0]],
                &[&data.present_sem],
                &[vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT],
                &[&data.render_sem],
                Some(&data.render_fence),
            )?;
            let result = self.graphics_queue.present(&self.swapchain, image_index, &[&data.render_sem]);
            match result {
                Ok(subopt) => {
                    if subopt {
                        suboptimal = true;
                    }
                }
                Err(e) => {
                    match e {
                        vk::Result::ERROR_OUT_OF_DATE_KHR => {
                            out_of_date = true;
                        }
                        e => {
                            return Err(anyhow::Error::from(e));
                        }
                    }
                }
            }

            if suboptimal || out_of_date {
                info!("Swapchain suboptimal or out of date at submit time... recreating swapchain");
                self.recreate_swapchain(self.win.inner_size())?;
            }
        }

        self.frame_counter += 1;
        Ok(())
    }
}

#[inline]
fn make_clear_color(colors: [f32; 4]) -> vk::ClearValue {
    vk::ClearValue { color: vk::ClearColorValue { float32: colors } }
}
