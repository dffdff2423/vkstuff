use crate::prelude::*;

use std::{
    io::{self, prelude::*},
    collections::HashMap,
    sync::Arc,
};

use vktools::{
    Device, CommandBuffer,
    ash::{prelude::*, vk},
    mem::{Sampler, Allocator},
    pipeline::DescriptorAllocator,
    util::Uploader,
};

use crate::{
    material::{self, Material, Mesh, MeshPushConstant, Texture, GlobalUniformBuffer, MeshVertex},
    util::transform_matrix,
};

type SpriteMaterial = Material<MeshPushConstant, material::MeshVertex, material::GlobalUniformBufferContents>;

struct SpriteRenderObject {
    mat: Arc<SpriteMaterial>,
    mesh: Arc<Mesh<MeshVertex>>,

    // Sampler & Texture need to live longer than this struct.
    tex: Option<Arc<Texture>>,
    samp: Option<Arc<Sampler>>,

    sets: Vec<vk::DescriptorSet>,
}

impl SpriteRenderObject {
    pub fn new(
        dev: &Device,
        desc_alloc: &mut DescriptorAllocator,
        mat: Arc<SpriteMaterial>,
        mesh: Arc<Mesh<MeshVertex>>,
        // FIXME: remove Options
        tex: Option<Arc<Texture>>,
        samp: Option<Arc<Sampler>>,
        global_uniform: &GlobalUniformBuffer,
    ) -> VkResult<Self> {
        let sets = unsafe { desc_alloc.allocate(&mat.layouts())? };

        let ubo_buf = vk::DescriptorBufferInfo {
            buffer: global_uniform.buffer().handle(),
            offset: 0,
            range: global_uniform.range() as u64,
        };

        let ubo_write = vk::WriteDescriptorSet {
            dst_set: sets[0],
            dst_binding: 0,
            descriptor_count: 1,
            p_buffer_info: &ubo_buf,
            descriptor_type: vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
            ..Default::default()
        };

        unsafe {
            dev.handle().update_descriptor_sets(&[ubo_write], &[]);
        }

        let mut me = Self { mat, mesh, tex: None, samp, sets };

        if me.samp.is_some() {
            if let Some(tex) = tex {
                me.set_texture(dev, tex);
            }
        }

        Ok(me)
    }

    fn set_texture(&mut self, dev: &Device, tex: Arc<Texture>) {
        let img_info = vk::DescriptorImageInfo {
            sampler: self.samp.as_ref().unwrap().handle(),
            image_view: tex.view.handle(),
            image_layout: tex.image.layout(),
        };

        let write_samp = vk::WriteDescriptorSet {
            dst_set: self.sets[1],
            dst_binding: 0,
            p_image_info: &img_info,
            descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
            descriptor_count: 1,
            ..Default::default()
        };

        unsafe {
            dev.handle().update_descriptor_sets(&[write_samp], &[]);
        }
        self.tex = Some(tex);
    }

    pub unsafe fn cmd_draw(&self, buf: &CommandBuffer, frame: usize, transform: Mat4, color: Vec3) {
        self.mat.cmd_bind(buf, MeshPushConstant { model: transform, color: color.extend(0.0) }, &self.sets, frame);
        self.mesh.cmd_draw(buf);
    }
}

pub struct SpriteRenderer {
    frame_idx: usize,
    ro_map: HashMap<String, SpriteRenderObject>,
    quad: Arc<Mesh<MeshVertex>>,
    mat: Arc<SpriteMaterial>,
    samp: Arc<Sampler>,
}

impl SpriteRenderer {
    pub fn new(alloc: Arc<Allocator>,  uploader: &Uploader, mat: Arc<SpriteMaterial>, samp: Arc<Sampler>) -> VkResult<Self> {
        let quad_vertecies = vec![
            // triangle 1
            MeshVertex { pos: vec3(0.0, 1.0, 0.0), tex_cord: vec2(0.0, 1.0), normal: vec3(1.0, 1.0, 1.0) },
            MeshVertex { pos: vec3(1.0, 0.0, 0.0), tex_cord: vec2(1.0, 0.0), normal: vec3(1.0, 1.0, 1.0) },
            MeshVertex { pos: vec3(0.0, 0.0, 0.0), tex_cord: vec2(0.0, 0.0), normal: vec3(1.0, 1.0, 1.0) },

            // triangle 2
            MeshVertex { pos: vec3(0.0, 1.0, 0.0), tex_cord: vec2(0.0, 1.0), normal: vec3(1.0, 1.0, 1.0) },
            MeshVertex { pos: vec3(1.0, 1.0, 0.0), tex_cord: vec2(1.0, 1.0), normal: vec3(1.0, 1.0, 1.0) },
            MeshVertex { pos: vec3(1.0, 0.0, 0.0), tex_cord: vec2(1.0, 0.0), normal: vec3(1.0, 1.0, 1.0) },
        ];

        let mut quad_mesh = Mesh::from_verticies(quad_vertecies);
        quad_mesh.upload(alloc, &uploader)?;
        quad_mesh.give_name("Quad Mesh")?;

        let quad_mesh = Arc::new(quad_mesh);

        Ok(Self { ro_map: HashMap::new(), frame_idx: usize::MAX, quad: quad_mesh, mat, samp })
    }

    #[inline]
    pub fn begin_frame(&mut self, idx: usize) {
        self.frame_idx = idx;
    }

    pub fn add_texture(&mut self, key: String, alloc: &Arc<Allocator>, uploader: &Uploader, desc: &mut DescriptorAllocator,
                       reader: impl Read, global_uniform: &GlobalUniformBuffer) -> anyhow::Result<()> {
        info!("Loading texture {key}");
        let tex = Arc::new(Texture::from_png(alloc.clone(), uploader, reader)?);
        self.ro_map.insert(key,
                           SpriteRenderObject::new(
                               alloc.dev(),
                               desc,
                               self.mat.clone(),
                               self.quad.clone(),
                               Some(tex),
                               Some(self.samp.clone()),
                               global_uniform)?);
        Ok(())
    }

    pub unsafe fn cmd_draw_spirite(&mut self, buf: &CommandBuffer, tex: &str, transform: Mat4, color: Vec3) {
        self.ro_map[tex].cmd_draw(buf, self.frame_idx, transform, color);
    }
}

pub trait GameObject {
    fn pos(&self) -> Vec2;
    fn size(&self) -> Vec2;
    fn velocity(&self) -> Vec2;
}

macro_rules! game_object {
    ($ty:ty) => {
        impl GameObject for $ty {
            #[inline]
            fn pos(&self) -> Vec2 {
                self.pos
            }

            #[inline]
            fn size(&self) -> Vec2 {
                self.size
            }

            #[inline]
            fn velocity(&self) -> Vec2 {
                self.velocity
            }
        }
    };
}

#[derive(Clone, Copy)]
pub struct SquareGameObject {
    pub pos: Vec2,
    pub size: Vec2,
    pub velocity: Vec2,
    pub color: Vec3,
    pub rotation: f32,
    pub is_solid: bool,
    pub destroyed: bool,
    pub sprite: &'static str,
}

impl SquareGameObject {
    #[inline]
    pub fn new(pos: Vec2, size: Vec2, is_solid: bool, color: Vec3, tex: &'static str) -> Self {
        Self {
            pos, size, velocity: vec2(0.0, 0.0), color, rotation: 0.0, is_solid,
            destroyed: false, sprite: tex,
        }
    }

    pub unsafe fn cmd_draw(&self, buf: &CommandBuffer, renderer: &mut SpriteRenderer) {
        renderer.cmd_draw_spirite(buf, self.sprite,
                                  transform_matrix(self.pos, self.size, self.rotation),
                                  self.color);
    }
}

game_object!(SquareGameObject);

#[derive(Clone, Copy)]
pub struct Ball {
    pub pos: Vec2,
    pub size: Vec2,
    pub velocity: Vec2,
    pub color: Vec3,
    pub sprite: &'static str,
    pub radius: f32,
    pub stopped: bool,
    pub sticky: bool,
    pub pass_through: bool,
}
impl Ball {
    pub fn new(pos: Vec2, radius: f32, velocity: Vec2) -> Self {
        Self {
            pos, size: vec2(radius * 2.0, radius * 2.0), velocity,
            color: Vec3::from([1.0; 3]), sprite: "ball", radius, stopped: true,
            sticky: false, pass_through: false,
        }
    }

    pub fn update_pos(&mut self, dt: f32, window_extent: vk::Extent2D) -> Vec2 {
        if !self.stopped {
            self.pos += self.velocity * dt;

            if self.pos.x <= 0.0 {
                self.velocity.x = -self.velocity.x;
                self.pos.x = 0.0;
            }
            if self.pos.x + self.size.x >= window_extent.width as f32 {
                self.velocity.x = -self.velocity.x;
                self.pos.x = window_extent.width as f32 - self.size.x;
            }
            if self.pos.y <= 0.0 {
                self.velocity.y = -self.velocity.y;
                self.pos.y = 0.0;
            }
        }
        return self.pos;
    }

    pub unsafe fn cmd_draw(&self, buf: &CommandBuffer, renderer: &mut SpriteRenderer) {
        renderer.cmd_draw_spirite(buf, self.sprite,
                                  transform_matrix(self.pos, self.size, 0.0),
                                  self.color);
    }
}

game_object!(Ball);

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum PowerUpType {
    Speed,
    Sticky,
    PassThrough,
    BigPaddle,
    Confuse,
    Chaos,
}

impl PowerUpType {
    pub fn as_str(&self) -> &'static str {
        match self {
            Self::Speed => "powerup-speed",
            Self::Sticky => "powerup-sticky",
            Self::PassThrough => "powerup-pass-through",
            Self::BigPaddle => "powerup-big-paddle",
            Self::Confuse => "powerup-confuse",
            Self::Chaos => "powerup-chaos",
        }
    }
}

#[derive(Clone, Copy)]
pub struct PowerUp {
    pub pos: Vec2,
    pub size: Vec2,
    pub velocity: Vec2,
    pub color: Vec3,
    pub rotation: f32,
    pub destroyed: bool,
    pub sprite: &'static str,
    pub ty: PowerUpType,
    pub duration: f32,
    pub activated: bool,
}

impl PowerUp {
    #[inline]
    pub fn new(ty: PowerUpType, duration: f32, pos: Vec2, color: Vec3, tex: &'static str) -> Self {
        Self {
            pos, size: vec2(70.0, 20.0), velocity: vec2(0.0, 500.0), color, rotation: 0.0,
            destroyed: false, sprite: tex, ty, duration, activated: false,
        }
    }
    pub unsafe fn cmd_draw(&self, buf: &CommandBuffer, renderer: &mut SpriteRenderer) {
        renderer.cmd_draw_spirite(buf, self.sprite,
                                  transform_matrix(self.pos, self.size, self.rotation),
                                  self.color);
    }
}

game_object!(PowerUp);

#[allow(unused)]
pub fn aabb_aabb_collide(left: impl GameObject, right: impl GameObject) -> bool {
    let collision_x = left.pos().x + left.size().x >= right.pos().x
        && right.pos().x + right.size().x >= left.pos().x;

    let collision_y = left.pos().y + left.size().y >= right.pos().y
        && right.pos().y + right.size().y >= left.pos().y;

    collision_x && collision_y
}

type Collision = (bool, Direction, Vec2);

pub fn aabb_ball_collide(aabb: impl GameObject, circle: Ball) -> Collision {
    let center = circle.pos + circle.radius;
    let aabb_half_extent = aabb.size() / 2.0;
    let aabb_center = aabb.pos() + aabb_half_extent;
    let diff = center - aabb_center;
    let clamped = diff.clamp(-aabb_half_extent, aabb_half_extent);
    let closest = aabb_center + clamped;
    let diff = closest - center;
    if diff.length() <= circle.radius {
        return (true, vector_direction(diff), diff);
    } else {
        return (false, Direction::Up, Vec2::from([0.0; 2]));
    }
}

fn vector_direction(target: Vec2) -> Direction {
    let directions = [
        vec2(0.0, 1.0), // up
        vec2(1.0, 0.0), // right
        vec2(0.0, -1.0), // down
        vec2(-1.0, 0.0), // left
    ];
    let mut max = 0.0;
    let mut best_match = -1isize;
    for i in 0..4 {
        let dot = target.normalize().dot(directions[i]);
        if dot > max {
            max = dot;
            best_match = i as isize;
        }
    }

    return best_match.into();
}

#[repr(usize)]
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Direction {
    Up = 0,
    Right,
    Down,
    Left,
}

impl From<isize> for Direction {
    fn from(i: isize) -> Self {
        use Direction::*;
        match i {
            0 => Up,
            1 => Right,
            2 => Down,
            3 => Left,
            _ => Up,
            //_ => panic!("Unexpected value {i}"),
        }
    }
}

pub struct Level {
    pub bricks: Vec<SquareGameObject>,
}

impl Level {
    pub fn load(mut reader: impl Read, extent: vk::Extent2D) -> io::Result<Self> {
        let mut lvl_str = String::new();
        reader.read_to_string(&mut lvl_str)?;

        let mut tile_data: Vec<Vec<u32>> = Vec::new();
        lvl_str.lines().for_each(|line| {
            let line = line.split_ascii_whitespace()
                .map(str::parse::<u32>)
                .map(Result::unwrap)
                .collect();
            tile_data.push(line);
        });

        let height = tile_data.len() as u32;
        let width = tile_data[0].len() as u32;

        debug!("Loading a {width}x{height} level");

        let unit_width = extent.width as f32 / width as f32;
        let unit_height = extent.height as f32 / height as f32;

        let mut bricks = Vec::new();

        for (y, row) in tile_data.into_iter().enumerate() {
            for (x, tile) in row.into_iter().enumerate() {
                let pos = vec2(unit_width * x as f32, unit_height * y as f32);
                let size = vec2(unit_width, unit_height);

                if tile == 1 {
                    let obj = SquareGameObject::new(pos, size, true, vec3(0.8, 0.8, 0.7), "block-solid");
                    bricks.push(obj);
                } else if tile > 1 {
                    let mut color = vec3(1.0, 1.0, 1.0);
                    if tile == 2 {
                        color = vec3(0.2, 0.6, 1.0);
                    } else if tile == 3 {
                        color = vec3(0.0, 0.7, 0.0);
                    } else if tile == 4 {
                        color = vec3(0.8, 0.8, 0.4);
                    } else if tile == 5 {
                        color = vec3(1.0, 0.5, 0.0);
                    }

                    let obj = SquareGameObject::new(pos, size, false, color, "block");
                    bricks.push(obj);
                }
            }
        }

        Ok(Self { bricks })
    }

    pub unsafe fn cmd_draw(&self, buf: &CommandBuffer, renderer: &mut SpriteRenderer) {
        for brick in self.bricks.iter() {
            if !brick.destroyed {
                brick.cmd_draw(buf, renderer);
            }
        }
    }

    pub fn is_completed(&self) -> bool {
        for brick in self.bricks.iter() {
            if !brick.is_solid && !brick.destroyed {
                return false;
            }
        }
        return true;
    }
}
