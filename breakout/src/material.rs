use crate::prelude::*;

use vktools::ash;
use ash::prelude::*;

use std::{
    io::prelude::*,
    mem::size_of,
    sync::Arc,
    marker::PhantomData,
};

use ash::vk;

use vktools::{
    CommandBuffer,
    mem::{Allocator, Buffer, MemoryUsage, Image, ImageView},
    util::{Uploader, to_byte_slice, pad_uniform_buffer_size},
    pipeline::{
        self, VertexInputDescription, ShaderModule, PipelineLayout, PushConstentInfo,
        RasterizationPipeline, DescriptorLayoutCache,
        uniform::{UniformContents, UniformBufferDynamic},
    },
    offset_of,
    format_vulkan_name,
};

use crate::util;

#[repr(C)]
#[derive(Clone, Copy)]
pub struct GlobalUniformBufferContents {
    pub proj: Mat4,
}

unsafe impl UniformContents for GlobalUniformBufferContents {}

pub type GlobalUniformBuffer = UniformBufferDynamic<GlobalUniformBufferContents>;

#[repr(C)]
#[derive(Clone, Copy)]
pub struct MeshPushConstant {
    pub model: Mat4,
    pub color: Vec4, // w is unused
}

// FIXME: accept more than 1 uniform buffer
pub struct Material<Push, Vertex: pipeline::Vertex, Uniform: UniformContents> {
    set_layouts: Vec<vk::DescriptorSetLayout>,
    push_consts_layout: Option<PushConstentInfo>,
    pipe_layout: PipelineLayout,
    pipe: RasterizationPipeline,

    _marker1: PhantomData<Push>,
    _marker2: PhantomData<Vertex>,
    _marker3: PhantomData<Uniform>,
}

impl<Push, Vertex: pipeline::Vertex, Uniform: UniformContents> Material<Push, Vertex, Uniform> {
    pub fn new(
        desc_cache: &mut DescriptorLayoutCache,
        dev: Arc<vktools::Device>,
        shaders: Vec<ShaderModule>,
        pipe_image_format: vk::Format,
        particle_blending: bool, // FIXME
    ) -> VkResult<Self> {
        // rust does not have static assertions
        assert!(size_of::<Push>() <= 128, "Push constants cannot be bigger than 128 bytes");

        let (push_consts_layout, desc_layouts) = pipeline::cauclulate_shader_information(shaders.as_slice()).unwrap();
        let set_layouts: Vec<vk::DescriptorSetLayout> = desc_layouts.into_iter()
            .map(|l| unsafe { desc_cache.create_layout(l) })
            .collect::<VkResult<Vec<_>>>()?;

        #[cfg(debug_assertions)]
        {
            let push_sz = size_of::<Push>();
            if push_sz != 0 {
                assert_eq!(push_consts_layout.unwrap().size as usize, push_sz
                        , "Cpu side push const must be the same size as SPIR-V");
            }
        }

        let pipe_layout = PipelineLayout::new(Arc::clone(&dev), &set_layouts, push_consts_layout)?;
        let pipe = RasterizationPipeline::builder()
            .set_shader_stages(shaders.iter().collect())
            .with_input_assembly(vk::PrimitiveTopology::TRIANGLE_LIST)
            .with_rasterization_state(vk::PolygonMode::FILL, vk::CullModeFlags::NONE)
            .with_multisample_state()
            .with_color_blend_attachment(true, particle_blending)
            .with_vertex_input(Vertex::get_input_description())
            .with_layout(&pipe_layout)
            .build(Arc::clone(&dev), pipe_image_format)?;

        //if size_of::<Vertex>() > 0 {
        //    pipe = pipe.with_vertex_input(Vertex::get_input_description());
        //}
        //let pipe = pipe.build(Arc::clone(&dev), pipe_image_format)?;

        Ok(Self {
            set_layouts, pipe_layout, pipe, push_consts_layout,
            _marker1: PhantomData, _marker2: PhantomData, _marker3: PhantomData,
        })
    }

    pub fn layouts(&self) -> &[vk::DescriptorSetLayout] {
        &self.set_layouts
    }

    // FIXME: binding the pipe, pushing consts, and binding descriptors should not be tied together.
    //        Or there should be some kind of draw state manager
    pub unsafe fn cmd_bind(
        &self,
        buf: &CommandBuffer,
        push_consts: Push,
        sets: &[vk::DescriptorSet],
        frame: usize,
    ) {
        let min_align = buf.dev().properties().limits.min_uniform_buffer_offset_alignment;
        let ubo_off = pad_uniform_buffer_size(min_align, (size_of::<Uniform>()) as u64) as u32 * frame as u32;
        buf.cmd_bind_descriptor_sets(vk::PipelineBindPoint::GRAPHICS, &self.pipe_layout, 0, sets, &[ubo_off]);
        self.pipe.cmd_bind(&buf);

        if let Some(push) = self.push_consts_layout {
            buf.cmd_push_constants(&self.pipe_layout, push.stage, push.offset, to_byte_slice(&[push_consts]))
        }
    }
}

pub struct Texture {
    pub image: Image,
    pub view: ImageView,
}

impl Texture {
    pub fn from_png(alloc: Arc<Allocator>, uploader: &Uploader, reader: impl Read) -> anyhow::Result<Texture> {
        let image = util::load_png(Arc::clone(&alloc), &uploader, reader)?;
        let view = image.create_view(vk::ImageViewType::TYPE_2D, Default::default())?;
        Ok(Self { image, view })
    }
}

#[repr(C)]
#[derive(Clone, Debug, PartialEq)]
pub struct MeshVertex {
    pub pos: Vec3,
    pub normal: Vec3,
    //pub color: Vec3,
    pub tex_cord: Vec2,
}

impl pipeline::Vertex for MeshVertex {
    fn get_input_description() -> VertexInputDescription {
        let binding = vk::VertexInputBindingDescription {
            binding: 0,
            stride: size_of::<Self>() as u32,
            input_rate: vk::VertexInputRate::VERTEX,
        };

        let pos_attr = vk::VertexInputAttributeDescription {
            binding: 0,
            location: 0,
            format: vk::Format::R32G32B32_SFLOAT,
            offset: offset_of!(MeshVertex, pos) as u32,
        };

        let normal_attr = vk::VertexInputAttributeDescription {
            binding: 0,
            location: 1,
            format: vk::Format::R32G32B32_SFLOAT,
            offset: offset_of!(MeshVertex, normal) as u32,
        };

        //let color_attr = vk::VertexInputAttributeDescription {
        //    binding: 0,
        //    location: 2,
        //    format: vk::Format::R32G32B32_SFLOAT,
        //    offset: offset_of!(Vertex, color) as u32,
        //};

        let tex_attr = vk::VertexInputAttributeDescription {
            binding: 0,
            location: 2,
            format: vk::Format::R32G32_SFLOAT,
            offset: offset_of!(MeshVertex, tex_cord) as u32,
        };

        let atters = vec![pos_attr, normal_attr, tex_attr];
        VertexInputDescription {
            bindings: vec![binding],
            attr: atters,
            flags: vk::PipelineVertexInputStateCreateFlags::empty(),
        }
    }
}

pub struct Mesh<T: pipeline::Vertex> {
    verticies: Vec<T>,
    vert_buf: Option<Buffer>,
    is_uploaded: bool,
}

impl<T: pipeline::Vertex> Mesh<T> {
    pub fn from_verticies(verticies: Vec<T>) -> Self {
        Self {
            verticies,
            vert_buf: None,
            is_uploaded: false,
        }
    }

    pub fn upload(&mut self, alloc: Arc<Allocator>, uploader: &Uploader) -> VkResult<()> {
        let sizeof_verticies = self.verticies.len() * size_of::<MeshVertex>();
        let mut stageing_buf = unsafe { Buffer::new(
            Arc::clone(&alloc),
            MemoryUsage::CpuOnly,
            vk::BufferUsageFlags::TRANSFER_SRC,
            sizeof_verticies as vk::DeviceSize)? };

        stageing_buf.map()?;
        {
            let bytes = to_byte_slice(&self.verticies);
            unsafe { stageing_buf.copy_to(bytes) };
        }
        stageing_buf.unmap();

        let gpu_buf = unsafe { Buffer::new(
            alloc,
            MemoryUsage::GpuOnly,
            vk::BufferUsageFlags::VERTEX_BUFFER | vk::BufferUsageFlags::TRANSFER_DST,
            sizeof_verticies as vk::DeviceSize)? };

        uploader.immediate_submit(|buf| {
            unsafe {
                buf.cmd_copy_buffer(&stageing_buf, &gpu_buf, 0, 0, sizeof_verticies as vk::DeviceSize);
            }
        })?;

        self.is_uploaded = true;
        self.vert_buf = Some(gpu_buf);

        Ok(())
    }

    pub unsafe fn cmd_draw(&self, buf: &CommandBuffer) {
        buf.cmd_bind_vertex_buffers(0, &[self.vert_buf.as_ref().unwrap()], &[0]);
        buf.cmd_draw(self.num_verticies() as u32, 1, 0, 0);
    }

    pub fn num_verticies(&self) -> usize {
        self.verticies.len()
    }

    pub fn give_name(&self, name: &str) -> VkResult<()> {
        format_vulkan_name!(self.vert_buf.as_ref().unwrap(), "{name} Vertex Buffer")
    }
}
