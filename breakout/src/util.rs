use crate::prelude::*;

use std::sync::Arc;
use std::io::prelude::*;

use vktools::ash;
use ash::vk;

use vktools::{
    mem::{Allocator, Image},
    util::Uploader,
};

pub fn load_png(alloc: Arc<Allocator>, uploader: &Uploader, reader: impl Read) -> anyhow::Result<Image> {
    let decoder = png::Decoder::new(reader);
    let mut reader = decoder.read_info()?;
    let mut buf = vec![0; reader.output_buffer_size()];
    let info = reader.next_frame(&mut buf)?;
    let width = info.width;
    let height = info.height;
    let bytes = &buf[..info.buffer_size()];

    assert_eq!(info.bit_depth, png::BitDepth::Eight);
    assert_eq!(info.color_type, png::ColorType::Rgba);
    let image = unsafe { uploader.upload_texture(alloc, bytes, vk::Format::R8G8B8A8_SRGB, width, height, false)? };
    Ok(image)
}

/// Transform matrix with pos, size, and rotation (in degrees)
#[inline]
pub fn transform_matrix(pos: Vec2, size: Vec2, rotate: f32) -> Mat4 {
    let mat = Mat4::from_translation(pos.extend(0.0));

    let mat = mat * Mat4::from_translation(vec3(0.5 * size.x, 0.5 * size.y, 0.0));
    let mat = mat * Mat4::from_rotation_z(rotate.to_radians());
    let mat = mat * Mat4::from_translation(vec3(-0.5 * size.x, -0.5 * size.y, 0.0));

    let mat = mat * Mat4::from_scale(size.extend(1.0));

    mat
}
