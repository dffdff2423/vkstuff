use crate::prelude::*;

use std::{
    io,
    mem::size_of,
    sync::Arc,
};

use vktools::{
    CommandBuffer,
    ash::vk,
    mem::{Sampler, Allocator},
    pipeline::{self, DescriptorAllocator, DescriptorLayoutCache, VertexInputDescription, ShaderModule},
    util::Uploader,
    offset_of,
};

use crate::{
    shaders,
    material::{Material, Mesh, Texture, GlobalUniformBuffer, GlobalUniformBufferContents},
    level::GameObject,
};

type ParticleMaterial = Material<ParticlePushConst, ParticleVertex, GlobalUniformBufferContents>;

#[repr(C)]
struct ParticleVertex {
    pos: Vec2,
    tex_cord: Vec2,
}

impl pipeline::Vertex for ParticleVertex {
    fn get_input_description() -> VertexInputDescription {
        let binding = vk::VertexInputBindingDescription {
            binding: 0,
            stride: size_of::<Self>() as u32,
            input_rate: vk::VertexInputRate::VERTEX,
        };

        let vertex_attr = vk::VertexInputAttributeDescription {
            binding: 0,
            location: 0,
            format: vk::Format::R32G32_SFLOAT,
            offset: offset_of!(ParticleVertex, pos) as u32,
        };

        let tex_cord_attr = vk::VertexInputAttributeDescription {
            binding: 0,
            location: 1,
            format: vk::Format::R32G32_SFLOAT,
            offset: offset_of!(ParticleVertex, tex_cord) as u32,
        };

        let attrs = vec![vertex_attr, tex_cord_attr];
        VertexInputDescription { bindings: vec![binding], attr: attrs, flags: vk::PipelineVertexInputStateCreateFlags::empty() }
    }
}

#[repr(C)]
struct ParticlePushConst {
    off: Vec2,
    color: Vec4,
}

#[derive(Clone, Copy, PartialEq)]
struct Particle {
    pos: Vec2,
    velocity: Vec2,
    color: Vec4,
    life: f32,
}

impl Default for Particle {
    fn default() -> Self {
        Self {
            pos: const_vec2!([0.0; 2]),
            velocity: const_vec2!([0.0; 2]),
            color: const_vec4!([1.0; 4]),
            life: 0.0,
        }
    }
}

pub struct ParticleEmmiter {
    quad: Mesh<ParticleVertex>,
    mat: ParticleMaterial,
    _tex: Arc<Texture>,
    particles: [Particle; 500],
    sets: Vec<vk::DescriptorSet>,
    last_used_particle: usize,
}

impl ParticleEmmiter {
    pub fn new(
        dev: Arc<vktools::Device>,
        alloc: Arc<Allocator>,
        desc_alloc: &mut DescriptorAllocator,
        desc_cache: &mut DescriptorLayoutCache,
        out_image_format: vk::Format,
        uploader: &Uploader,
        tex: Arc<Texture>,
        samp: Arc<Sampler>,
        global_uniform: &GlobalUniformBuffer,
    ) -> anyhow::Result<Self> {
        let quad_vertecies = vec![
            // triangle 1
            ParticleVertex { pos: vec2(0.0, 1.0), tex_cord: vec2(0.0, 1.0) },
            ParticleVertex { pos: vec2(1.0, 0.0), tex_cord: vec2(1.0, 0.0) },
            ParticleVertex { pos: vec2(0.0, 0.0), tex_cord: vec2(0.0, 0.0) },

            // triangle 2
            ParticleVertex { pos: vec2(0.0, 1.0), tex_cord: vec2(0.0, 1.0) },
            ParticleVertex { pos: vec2(1.0, 1.0), tex_cord: vec2(1.0, 1.0) },
            ParticleVertex { pos: vec2(1.0, 0.0), tex_cord: vec2(1.0, 0.0) },
        ];

        let mut quad_mesh = Mesh::from_verticies(quad_vertecies);
        quad_mesh.upload(Arc::clone(&alloc), &uploader)?;
        quad_mesh.give_name("Particle Quad Mesh")?;

        let shaders = vec![
            ShaderModule::from_reader(Arc::clone(&dev), vk::ShaderStageFlags::VERTEX
                                      , &mut io::Cursor::new(shaders::PARTICLE_VERT))?,
            ShaderModule::from_reader(Arc::clone(&dev), vk::ShaderStageFlags::FRAGMENT
                                      , &mut io::Cursor::new(shaders::PARTICLE_FRAG))?,
        ];

        let mat = Material::new(
            desc_cache, Arc::clone(&dev), shaders, out_image_format, true
        )?;

        let sets = unsafe { desc_alloc.allocate(&mat.layouts())? };

        let ubo_write_buf = vk::DescriptorBufferInfo {
            buffer: global_uniform.buffer().handle(),
            offset: 0,
            range: global_uniform.range() as vk::DeviceSize,
        };

        let ubo_write = vk::WriteDescriptorSet {
            dst_set: sets[0],
            dst_binding: 0,
            descriptor_count: 1,
            p_buffer_info: &ubo_write_buf,
            descriptor_type: vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
            ..Default::default()
        };

        let write_samp_image = vk::DescriptorImageInfo {
            sampler: samp.handle(),
            image_view: tex.view.handle(),
            image_layout: tex.image.layout(),
        };

        let write_samp = vk::WriteDescriptorSet {
            dst_set: sets[1],
            dst_binding: 0,
            p_image_info: &write_samp_image,
            descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
            descriptor_count: 1,
            ..Default::default()
        };

        unsafe { dev.handle().update_descriptor_sets(&[write_samp, ubo_write], &[]) };

        Ok(Self {
            quad: quad_mesh,
            mat, _tex: tex,
            particles: [Particle::default(); 500],
            sets,
            last_used_particle: 0,
        })
    }

    pub fn update(&mut self, dt: f32, obj: &impl GameObject, rng: &mut impl rand_core::RngCore) {
        const NUM_NEW_PARTICLES: usize = 2;

        for _ in 0..NUM_NEW_PARTICLES {
            let unused = self.find_unused_particle();
            self.spawn_particle(rng, unused, obj, vec2(0.0, 0.0));
        }

        for particle in self.particles.iter_mut() {
            particle.life -= dt;
            if particle.life > 0.0 {
                particle.pos -= particle.velocity * dt;
                particle.color.w -= dt * 2.5;
                if particle.color.w < 0.0 {
                    particle.life = -1.0;
                }
            }
        }
    }

    fn spawn_particle(&mut self, rng: &mut impl rand_core::RngCore,
                      idx: usize, obj: &impl GameObject, off: Vec2) {
        let mut particle = &mut self.particles[idx];
        //let rand_off = ((rng.next_u32() % 100) - 50) as f32 / 10.0;
        let rand_off = (rng.next_u32() % 100) as f32 / 10.0;
        let color = 0.5 + ((rng.next_u32() % 100) as f32 / 100.0);
        particle.pos = obj.pos() + rand_off + off;
        particle.color = vec4(color, color, color, 1.0);
        particle.life = 5.0;
        particle.velocity = obj.velocity() * 0.1;
    }

    fn find_unused_particle(&mut self) -> usize {
        for i in self.last_used_particle..self.particles.len() {
            if self.particles[i].life <= 0.0 {
                self.last_used_particle = i;
                return i;
            }
        }

        for i in 0..self.particles.len() {
            if self.particles[i].life <= 0.0 {
                self.last_used_particle = i;
                return i;
            }
        }

        self.last_used_particle = 0;
        return 0;
    }

    pub unsafe fn cmd_draw(&self, buf: &CommandBuffer, frame: usize) {
        for p in self.particles {
            if p.life > 0.0 {
                // FIXME: don't bind for every particle
                let push = ParticlePushConst {
                    off: p.pos,
                    color: p.color,
                };
                self.mat.cmd_bind(buf, push, &self.sets, frame);
                self.quad.cmd_draw(buf);
            }
        }
    }
}
