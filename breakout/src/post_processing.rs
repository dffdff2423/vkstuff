use crate::prelude::*;

use std::{
    sync::Arc,
    io,
};

use vktools::ash::{vk, prelude::*};

use vktools::{
    CommandBuffer, Device,
    pipeline::{
        ShaderModule, DescriptorLayoutCache, DescriptorAllocator,
        uniform::{UniformContents, UniformBufferDynamic},
    },
    mem::Sampler,
};

use crate::{
    material::{Material, Texture}
};

#[repr(C)]
#[derive(Clone, Copy, Default)]
pub struct PostProcessingUniformContents {
    pub offsets: [Vec2; 9],
    pub edge_kernel: [i32; 9],
    pub blur_kernel: [f32; 9],
    pub chaos: vk::Bool32,
    pub confuse: vk::Bool32,
    pub shake: vk::Bool32,
    pub time: f32,
}

unsafe impl UniformContents for PostProcessingUniformContents {}

pub type PostProcessingUniformBuffer = UniformBufferDynamic<PostProcessingUniformContents>;

pub struct DummyPushConst; // FIXME: this is such an awful hack
pub struct DummyVertex;
impl vktools::pipeline::Vertex for DummyVertex {
    fn get_input_description() -> vktools::pipeline::VertexInputDescription {
        vktools::pipeline::VertexInputDescription{
            bindings: Vec::new(),
            attr: Vec::new(),
            flags: Default::default(),
        }
    }
}

pub type PostProcessingMaterial = Material<DummyPushConst, DummyVertex, PostProcessingUniformContents>;

pub struct PostProcessing {
    mat: Arc<PostProcessingMaterial>,
    sets: Vec<vk::DescriptorSet>,
    samp: Arc<Sampler>,
}

impl PostProcessing {
    pub fn new(
        dev: &Device,
        mat: Arc<PostProcessingMaterial>,
        samp: Arc<Sampler>,
        desc_alloc: &mut DescriptorAllocator,
        ubo: &PostProcessingUniformBuffer,
    ) -> VkResult<Self> {
        let sets = unsafe { desc_alloc.allocate(&mat.layouts())? };

        let ubo = vk::DescriptorBufferInfo {
            buffer: ubo.buffer().handle(),
            offset: 0,
            range: ubo.range(),
        };

        let ubo_write = vk::WriteDescriptorSet {
            dst_set: sets[0],
            dst_binding: 1,
            p_buffer_info: &ubo,
            descriptor_type: vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
            descriptor_count: 1,
            ..Default::default()
        };
        unsafe {
            dev.handle().update_descriptor_sets(&[ubo_write], &[]);
        }

        Ok(Self {mat, sets, samp})
    }

    pub fn write_image(&self, dev: &Device, input_image: &Texture) {
        let image =  vk::DescriptorImageInfo {
            image_view: input_image.view.handle(),
            sampler: self.samp.handle(),
            image_layout: input_image.image.layout(),
        };

        let image_write = vk::WriteDescriptorSet {
            dst_set: self.sets[0],
            dst_binding: 0,
            p_image_info: &image,
            descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
            descriptor_count: 1,
            ..Default::default()
        };

        unsafe {
            dev.handle().update_descriptor_sets(&[image_write], &[]);
        }
    }

    pub unsafe fn cmd_do(&self, buf: &CommandBuffer, frame: usize) {
        self.mat.cmd_bind(buf, DummyPushConst, &self.sets, frame);
        buf.cmd_draw(3, 1, 0, 0);
    }
}

pub fn create_material_for_post_proccessing(
        dev: Arc<Device>,
        desc_cache: &mut DescriptorLayoutCache,
        out_image_format: vk::Format,
) -> anyhow::Result<PostProcessingMaterial> {
    let shaders = vec![
        ShaderModule::from_reader(Arc::clone(&dev), vk::ShaderStageFlags::VERTEX
                , &mut io::Cursor::new(crate::shaders::POST_PROC_VERT))?,
        ShaderModule::from_reader(Arc::clone(&dev), vk::ShaderStageFlags::FRAGMENT
                , &mut io::Cursor::new(crate::shaders::POST_PROC_FRAG))?,
    ];
    PostProcessingMaterial::new(desc_cache, dev, shaders, out_image_format, false).map_err(|e| anyhow::Error::from(e))
}
