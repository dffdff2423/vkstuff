use crate::prelude::*;

use std::{
    collections::HashMap,
    io,
    sync::Arc,
};

use glam::i32::*;

use vktools::{
    ash::vk,
    Device, Nameable, format_vulkan_name, CommandBuffer,
    mem::{Allocator, Sampler},
    pipeline::{self, ShaderModule, DescriptorLayoutCache, DescriptorAllocator},
    util::Uploader,
};

use crate::{
    resources, shaders,
    material::{Material, GlobalUniformBuffer, GlobalUniformBufferContents, Texture},
};

#[allow(unused)]
#[repr(C)]
struct PushConst {
    text_color: Vec4, // w unused
    verticies: [Vertex; 6],
}

#[derive(Clone, Copy)]
#[allow(unused)]
#[repr(C)]
struct Vertex {
    pos: Vec2,
    tex_cord: Vec2,
}

impl pipeline::Vertex for Vertex {
    fn get_input_description() -> pipeline::VertexInputDescription {
        pipeline::VertexInputDescription {
            bindings: vec![],
            attr: vec![],
            flags: vk::PipelineVertexInputStateCreateFlags::empty(),
        }
    }
}

type TextMaterial = Material<PushConst, Vertex, GlobalUniformBufferContents>;

struct Glyph {
    #[allow(unused)]
    image: Texture,
    descriptor: vk::DescriptorSet,
    size: IVec2,
    bearing: IVec2,
    advance: u64,
}

pub struct TextRenderer {
    mat: TextMaterial,
    glyphs: HashMap<char, Glyph>,
    #[allow(unused)]
    samp: Sampler,
    #[allow(unused)]
    lib: freetype::Library,
    uniform_set: vk::DescriptorSet,
}

impl TextRenderer {
    pub fn new(
        dev: Arc<Device>,
        alloc: Arc<Allocator>,
        desc_cache: &mut DescriptorLayoutCache,
        desc_alloc: &mut DescriptorAllocator,
        out_image_format: vk::Format,
        uploader: &Uploader,
        uniform: &GlobalUniformBuffer,
    ) -> anyhow::Result<Self> {
        use std::rc::Rc;
        info!("Loading font");

        let lib = freetype::Library::init()?;
        let face = lib.new_memory_face::<Rc<Vec<u8>>>(Rc::new(resources::JETBRAINSMONO_REGULAR_TTF.to_owned()), 0)?;
        face.set_pixel_sizes(0, 48)?;

        let samp = Sampler::new(Arc::clone(&dev), vk::Filter::LINEAR,
                vk::SamplerAddressMode::REPEAT, vk::FALSE, None, None)?;


        let shaders = vec![
            ShaderModule::from_reader(Arc::clone(&dev), vk::ShaderStageFlags::VERTEX
                    , &mut io::Cursor::new(shaders::TEXT_VERT))?,

            ShaderModule::from_reader(Arc::clone(&dev), vk::ShaderStageFlags::FRAGMENT
                    , &mut io::Cursor::new(shaders::TEXT_FRAG))?,
        ];

        let mat = Material::new(desc_cache, Arc::clone(&dev), shaders, out_image_format, false)?;
        let layouts = mat.layouts();

        // Currently we only support ascii characters that are readable.
        const GLYPHS: &str =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!№;%:?*()_+-=.,/|\"'@#$^&{}[] ";

        let gpu_glyphs = GLYPHS.chars().map(|char| {
            face.load_char(char as usize, freetype::face::LoadFlag::RENDER)?;
            let glyph = face.glyph();
            let bm = glyph.bitmap();

            let width = u32::max(bm.width() as u32, 1);
            let height = u32::max(bm.rows() as u32, 1);
            let bitmap = if bm.width() < 1 || bm.rows() < 1 { &[0] } else { bm.buffer() };

            // FIXME: should only be 1 image for all glyphs
            let image = unsafe {
                uploader.upload_texture(Arc::clone(&alloc), bitmap,
                        vk::Format::R8_SRGB, width, height, false)
            }.context("Failed to create image for glyph")?;

            format_vulkan_name!(image, "Glyph `{char}` Image")?;

            let view = image.create_view(vk::ImageViewType::TYPE_2D, vk::ComponentMapping::default())?;

            let tex = Texture {
                image,
                view,
            };

            let descriptor = unsafe { desc_alloc.allocate(&[layouts[1]]) }?[0];

            let img_write = vk::DescriptorImageInfo {
                sampler: samp.handle(),
                image_view: tex.view.handle(),
                image_layout: tex.image.layout(),
            };
            let write = vk::WriteDescriptorSet {
                dst_set: descriptor,
                dst_binding: 0,
                descriptor_count: 1,
                p_image_info: &img_write,
                descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
                ..Default::default()
            };

            unsafe {
                dev.handle().update_descriptor_sets(&[write], &[]);
            }

            Ok((char, Glyph {
                image: tex,
                descriptor,
                size: ivec2(bm.width(), bm.rows()),
                bearing: ivec2(glyph.bitmap_left(), glyph.bitmap_top()),
                advance: glyph.advance().x as u64,
            }))
        }).collect::<anyhow::Result<HashMap<char, Glyph>>>()?;

        let uniform_set = unsafe { desc_alloc.allocate(&[layouts[0]]) }?[0];

        let uniform_write_buf = vk::DescriptorBufferInfo {
            buffer: uniform.buffer().handle(),
            offset: 0,
            range: uniform.range(),
        };

        let uniform_write = vk::WriteDescriptorSet{
            dst_set: uniform_set,
            dst_binding: 0,
            descriptor_count: 1,
            p_buffer_info: &uniform_write_buf,
            descriptor_type: vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
            ..Default::default()
        };

        unsafe { dev.handle().update_descriptor_sets(&[uniform_write], &[]) };

        Ok(Self {
            lib,
            mat,
            glyphs: gpu_glyphs,
            samp,
            uniform_set,
        })
    }

    pub unsafe fn cmd_draw_line(&mut self, buf: &CommandBuffer, text: &str,
                pos: Vec2, scale: f32, color: Vec3, frame: usize) {
        let mut pos = pos;
        for c in text.chars() {
            let glyph = &self.glyphs[&c];

            let xpos = pos.x + glyph.bearing.x as f32 * scale;
            // H is the tallist chariter because we only support ASCII
            let ypos = pos.y + (self.glyphs[&'H'].bearing.y - glyph.bearing.y) as f32 * scale;

            let w = glyph.size.x as f32 * scale;
            let h = glyph.size.y as f32 * scale;

            let verticies = [
                Vertex { pos: vec2(xpos,     ypos + h), tex_cord: vec2(0.0, 1.0) },
                Vertex { pos: vec2(xpos + w, ypos    ), tex_cord: vec2(1.0, 0.0) },
                Vertex { pos: vec2(xpos,     ypos    ), tex_cord: vec2(0.0, 0.0) },

                Vertex { pos: vec2(xpos,     ypos + h), tex_cord: vec2(0.0, 1.0) },
                Vertex { pos: vec2(xpos + w, ypos + h), tex_cord: vec2(1.0, 1.0) },
                Vertex { pos: vec2(xpos + w, ypos    ), tex_cord: vec2(1.0, 0.0) },
            ];

            let push = PushConst {
                text_color: color.extend(0.0),
                //verticies: [Vertex { pos: vec2(1.0, 1.0), tex_cord: vec2(1.0, 1.0) }; 6]
                verticies,
            };

            // TODO: only bind once
            self.mat.cmd_bind(buf, push, &[self.uniform_set, glyph.descriptor], frame);
            buf.cmd_draw(verticies.len() as u32, 1, 0, 0);
            pos.x += (glyph.advance >> 6) as f32 * scale;
            //pos.x += 20.0;
        }
    }
}
