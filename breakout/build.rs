//! quick and dirty script to compile shaders

use std::env;
use std::fmt::Write;

use xshell::{cmd, Shell};

fn main() -> anyhow::Result<()> {
    println!("cargo:rerun-if-changed=build.rs");

    let sh = Shell::new()?;

    let mut shaders_file_text = String::new();
    for path in sh.read_dir("shaders")? {
        let shader = path.file_name().unwrap().to_str().unwrap();
        println!("cargo:rerun-if-changed=shaders/{shader}");
        let out_path = format!("{}/{}.spv", env::var("OUT_DIR").unwrap(), shader);
        cmd!(sh, "glslc -O0 -g shaders/{shader} -o {out_path}").run().unwrap();

        let const_name = to_screaming_snake(shader);
        writeln!(shaders_file_text, "pub const {const_name}: &[u8] = include_bytes!(\"{out_path}\");")?;
    }

    sh.write_file(format!("{}/shaders.rs", env::var("OUT_DIR").unwrap()), shaders_file_text)?;

    let mut resource_file_text = String::new();
    for path in sh.read_dir("resources")? {
        let pstr = path.to_str().unwrap();
        println!("cargo:rerun-if-changed={pstr}");

        let fpath = path.file_name().unwrap().to_str().unwrap();
        let var_name = to_screaming_snake(fpath);
        writeln!(resource_file_text, "pub const {var_name}: &[u8] = include_bytes!(\"{pstr}\");")?;
    }
    sh.write_file(format!("{}/resources.rs", env::var("OUT_DIR").unwrap()), resource_file_text)?;

    Ok(())
}

fn to_screaming_snake(str: &str) -> String {
    str.chars().map(|c|{
        match c {
            c if c.is_ascii_alphabetic() => c.to_ascii_uppercase(),
            '-' => '_',
            '.' => '_',
            c => c,
        }
    }).collect()
}
