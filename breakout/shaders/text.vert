#version 450

layout (location = 0) out vec2 out_tex_cord;

layout (set = 0, binding = 0) uniform GlobalUinformBuffer {
	mat4 proj;
} global_uniform;

layout (push_constant) uniform constants {
	vec4 text_color; // x, y, = pos; z, w = tex
	vec4 verticies[6];
} push_const;

void main()
{
	gl_Position = global_uniform.proj * vec4(push_const.verticies[gl_VertexIndex].xy, 0.0, 1.0);
	out_tex_cord = push_const.verticies[gl_VertexIndex].zw;
}
