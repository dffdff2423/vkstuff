// This file is based off samples from:
// https://learnopengl.com/In-Practice/2D-Game/Postprocessing
// made by Joey de Vries and is licesned under CC-BY-NC. See CREDITS.txt for
// the full copyright

#version 450

#extension GL_EXT_scalar_block_layout : enable

layout (location = 0) in vec2 tex_cord;

layout (location = 0) out vec4 out_color;

layout (set = 0, binding = 0) uniform sampler2D scene;

layout (std430, set = 0, binding = 1) uniform PostProcUniform {
	vec2 offsets[9];
	int edge_kernel[9];
	float blur_kernel[9];
	bool chaos;
	bool confuse;
	bool shake;
	float time;
} inputs;

void main()
{
	out_color = vec4(0.f);
	vec3 samp[9];

	if (inputs.chaos || inputs.shake) {
		for (int i = 0; i < 9; ++i)
			samp[i] = vec3(texture(scene, tex_cord + inputs.offsets[i]));
	}

	if (inputs.chaos) {
		for (int i = 0; i < 9; ++i)
			out_color += vec4(samp[i] * inputs.edge_kernel[i], 0.f);
		out_color.a = 1.0f;
	} else if (inputs.confuse) {
		out_color = vec4(1.0 - texture(scene, tex_cord).xyz, 1.0);
	} else if (inputs.shake) {
		for (int i = 0; i < 9; ++i)
			out_color += vec4(samp[i] * inputs.blur_kernel[i], 0.0);
		out_color.a = 1.0f;
	} else {
		out_color = texture(scene, tex_cord);
	}
}
