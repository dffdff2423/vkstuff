#version 450

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 tex_cord;

layout (location = 0) out vec3 out_color;
layout (location = 1) out vec2 out_tex_cord;

layout (set = 0, binding = 0) uniform GlobalUinformBuffer {
	mat4 proj;
} global_uniform;

layout (push_constant) uniform constants {
	mat4 model;
	vec4 color; // w is unused
} push_const;

void main()
{
	out_tex_cord = tex_cord;
	gl_Position = global_uniform.proj * push_const.model * vec4(position, 1.0);
}
