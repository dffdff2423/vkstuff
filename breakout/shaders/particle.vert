// This file is based off samples from:
// https://learnopengl.com/In-Practice/2D-Game/Particles/
// made by Joey de Vries and is licesned under CC-BY-NC. See CREDITS.txt for
// the full copyright
#version 450

layout (location = 0) in vec2 pos;
layout (location = 1) in vec2 tex_cord;

layout (location = 0) out vec2 out_tex_cord;
layout (location = 1) out vec4 out_color;

layout (set = 0, binding = 0) uniform GlobalUinformBuffer {
	mat4 proj;
} global_uniform;

layout (push_constant) uniform constants {
	vec2 off;
	vec4 color;
} push_const;

void main()
{
	float scale = 10.f;
	out_tex_cord = tex_cord;
	out_color = push_const.color;
	gl_Position = global_uniform.proj * vec4((pos * scale) + push_const.off, 0.0, 1.0);
}
