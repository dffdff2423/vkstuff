#version 450

layout (location = 0) in vec2 tex_cord;

layout (location = 0) out vec4 out_color;

layout (set = 1, binding = 0) uniform sampler2D text;

layout (push_constant) uniform constants {
	vec4 text_color;
	vec4 verticies[6];
} push_const;

void main()
{
	vec4 tex_color = vec4(1.0, 1.0, 1.0, texture(text, tex_cord).r);
	out_color = vec4(push_const.text_color.xyz, 1.0) * tex_color;
}
