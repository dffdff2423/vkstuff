// This file is based off samples from:
// https://learnopengl.com/In-Practice/2D-Game/Particles/
// made by Joey de Vries and is licesned under CC-BY-NC. See CREDITS.txt for
// the full copyright
#version 450

layout (location = 0) in vec2 tex_cord;
layout (location = 1) in vec4 color;

layout (location = 0) out vec4 out_color;

layout (set = 1, binding = 0) uniform sampler2D image;

void main()
{
	out_color = texture(image, tex_cord) * color;
}
