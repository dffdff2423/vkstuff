#version 450

layout (location = 0) in vec3 color;
layout (location = 1) in vec2 tex_cord;

layout (location = 0) out vec4 out_color;

layout (set = 1, binding = 0) uniform sampler2D image;

layout (push_constant) uniform constants {
	mat4 model;
	vec4 color; // w is unused
} push_const;

void main()
{
	out_color = vec4(push_const.color.xyz, 1.0) * texture(image, tex_cord);
}
