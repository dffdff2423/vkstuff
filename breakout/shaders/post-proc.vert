// This file is based off samples from:
// https://learnopengl.com/In-Practice/2D-Game/Postprocessing
// made by Joey de Vries and is licesned under CC-BY-NC. See CREDITS.txt for
// the full copyright

#version 450

#extension GL_EXT_scalar_block_layout : enable

layout (location = 0) out vec2 tex_cord;

layout (set = 0, binding = 0) uniform sampler2D scene; // DO NOT USE

layout (std430, set = 0, binding = 1) uniform PostProcUniform {
	vec2 offsets[9];
	int edge_kernel[9];
	float blur_kernel[9];
	bool chaos;
	bool confuse;
	bool shake;
	float time;
} inputs;

void main()
{
	const vec2 triangle_vert[3] = vec2[3](
		vec2(-1.0, -1.0),
		vec2(3.0, -1.0),
		vec2(-1.0, 3.0)
	);

	vec2 pos = triangle_vert[gl_VertexIndex];

	const vec2 triangle_tex[3] = vec2[3](
		vec2(0.0, 0.0),
		vec2(2.0, 0.0),
		vec2(0.0, 2.0)
	);

	vec2 tex = triangle_tex[gl_VertexIndex];


	if (inputs.chaos) {
		const float strength = 0.3f;
		tex = vec2(tex.x + sin(inputs.time) * strength, tex.y + cos(inputs.time) * strength);
	} else if (inputs.confuse) {
		tex = 1.0f - tex;
	}

	tex_cord = tex;

	if (inputs.shake) {
		const float strength = 0.01;
		pos.x += cos(inputs.time * 30) * strength;
		pos.y += cos(inputs.time * 45) * strength;
	}

	gl_Position = vec4(pos, 0.0, 1.0);
}
