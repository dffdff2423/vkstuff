# Breakout

Breakout heavily inspired by the [learnopengl.com breakout game](https://learnopengl.com/In-Practice/2D-Game/Breakout)
but using vulkan instead of opengl and rust instead of c++. It copies most of
the gameplay, algorithms, and levels from the original tutorial. Currently there
exists a small bug with text rendering.

## Building

### Linux

- Requires the vulkan SDK to be installed, on arch the package group is called vulkan-devel.

```sh
cargo build --release --no-default-features --features=x11,wayland,linked-vulkan
```

### Windows on linux

- Requires mingw g++ and binutils to be installed, on arch the package group is called mingw-w64-toolchain.

```sh
CXXFLAGS="-Ivendor/Vulkan-Headers/include --static" cargo build \
    --no-default-features \
    --features loaded-vulkan \
    --release \
    --target x86_64-pc-windows-gnu

# This should be run from the root of this project and may be distro specific; tested on arch.
cp /usr/x86_64-w64-mingw32/bin/{libgcc_s_seh-1,libstdc++-6,libwinpthread-1}.dll ./target/x86_64-pc-windows-gnu/release
```
